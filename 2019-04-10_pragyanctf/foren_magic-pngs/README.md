# Magic PNGs (foren)

> Can you help me open this zip file?
> I seem to have forgotten its password.
> I think the image file has something to do with it.

```
# file tryme.zip 
tryme.zip: Zip archive data, at least v1.0 to extract

# file you_cant_see_me.png 
you_cant_see_me.png: data
```
Los inputs son dos ficheros: un ZIP y lo que parece un PNG corrupto.
El contenido del ZIP es un fichero `flag.txt` protegido con clave por lo que intuyo que
esa password debe salir de la imagen, vamos al lio.

### Analisis inicial del PNG

Lo primero es corregir la cabecera a mano, a mi me gusta usar PCRT por rapidez:
```
# python PCRT.py -i ../you_cant_see_me.png

	 ____   ____ ____ _____ 
	|  _ \ / ___|  _ \_   _|
	| |_) | |   | |_) || |  
	|  __/| |___|  _ < | |  
	|_|    \____|_| \_\|_|  

	PNG Check & Repair Tool 

Project address: https://github.com/sherlly/PCRT
Author: sherlly
Version: 1.1
	
[Detected] Wrong PNG header!
File header: 89504E472E0A2E0A
Correct header: 89504E470D0A1A0A
[Notice] Auto fixing? (y or n) [default:y] y
[Finished] Now header:89504E470D0A1A0A
[Finished] Correct IHDR CRC (offset: 0x1D): 42DFF335
[Finished] IHDR chunk check complete (offset: 0x8)
[Detected] Error IDAT chunk data length! (offset: 0xX5)
chunk length:44AE4260
actual length:0
[Notice] Try fixing it? (y or n) [default:y] n
```

Ya veo que hay mas problemas en los chunks. Compruebo que ya detecta que es un PNG:
```
# file fixed.png 
fixed.png: PNG image data, 205 x 246, 8-bit colormap, non-interlaced
```

Realizo un testeo de los chunks que tienen problemas:
```
# pngcheck -v fixed.png 
File: fixed.png (6163 bytes)
  chunk IHDR at offset 0x0000c, length 13
    205 x 246 image, 8-bit palette, non-interlaced
  chunk gAMA at offset 0x00025, length 4: 0.45455
  chunk cHRM at offset 0x00035, length 32
    White x = 0.3127 y = 0.329,  Red x = 0.64 y = 0.33
    Green x = 0.3 y = 0.6,  Blue x = 0.15 y = 0.06
  chunk PLTE at offset 0x00061, length 132: 44 palette entries
  chunk bKGD at offset 0x000f1, length 1
    index = 0
  chunk pHYs at offset 0x000fe, length 9: 2835x2835 pixels/meter (72 dpi)
  chunk idat at offset 0x00113, length 5737:  illegal reserved-bit-set chunk
ERRORS DETECTED in fixed.png

# pnginfo fixed.png 
fixed.png...
libpng warning: idat: CRC error
libpng warning: tEXt: CRC error
libpng warning: tEXt: CRC error
libpng error: IEND: out of place
Could not set PNG jump value
```

### Correccion de chunks

Especificaciones: <https://www.w3.org/TR/PNG/#11IDAT>

Despues de muchas pruebas, si vamos a la documentacion del estandar (https://www.w3.org/TR/PNG/#11IDAT), el chunk que da problemas tiene como tipo **idat** (0x69646174), en minusculas, en vez de **IDAT** (0x49444154). Con lo cual, cambiando estos 4 valores hex se solucionan todos los problemas y se renderiza la imagen.

El hexdump de como estaba:
```
000000f0  01 62 4b 47 44 00 88 05  1d 48 00 00 00 09 70 48  |.bKGD....H....pH|
00000100  59 73 00 00 0b 13 00 00  0b 13 01 00 9a 9c 18 00  |Ys..............|
00000110  00 16 69 69 64 61 74 78  da dd 1d 89 b6 aa 38 4c  |..iidatx......8L|
00000120  11 14 14 15 51 c0 0d 17  44 51 ef ff ff df 14 5a  |....Q...DQ.....Z|
00000130  ba d1 42 8b 0b be c9 39  73 e6 ea 93 36 69 d3 ec  |..B....9s...6i..|
```

Como se corrije:
```
000000f0  01 62 4b 47 44 00 88 05  1d 48 00 00 00 09 70 48  |.bKGD....H....pH|
00000100  59 73 00 00 0b 13 00 00  0b 13 01 00 9a 9c 18 00  |Ys..............|
00000110  00 16 69 49 44 41 54 78  da dd 1d 89 b6 aa 38 4c  |..iIDATx......8L|
00000120  11 14 14 15 51 c0 0d 17  44 51 ef ff ff df 14 5a  |....Q...DQ.....Z|
00000130  ba d1 42 8b 0b be c9 39  73 e6 ea 93 36 69 d3 ec  |..B....9s...6i..|
```

Abriendo la imagen se observa el texto `h4CK3RM4n`.

Probando a extraer con la pass el contenido del ZIP no funciona, algo va mal.

Viendo la pista del reto que habian abierto a todos los players:
```
You may have to hash the secret word to get the flag...
```

Cifro con MD5 `2c919f82ee2ed6985d5c5e275d67e4f8` que resulta ser la pass valida.

```
# unzip -P 2c919f82ee2ed6985d5c5e275d67e4f8 tryme.zip 
Archive:  tryme.zip
 extracting: flag.txt  

# cat flag.txt 
pctf{y0u_s33_m33_n0w!}
```

Solucion: `pctf{y0u_s33_m33_n0w!}`












