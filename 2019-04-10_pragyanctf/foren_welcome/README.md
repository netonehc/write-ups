# Welcome (foren)

> Do you think this is a normal image? No! Dig deeper to find out more.....

En enunciado ya nos dice que la imagen contiene mas informacion y que miremos hasta
el fondo, tipico stack de ficheros que se puede verificar con tools como binwalk.

```
# file welcome.jpeg 
welcome.jpeg: JPEG image data, JFIF standard 1.01, resolution (DPI), density 72x72, segment length 16, progressive, precision 8, 271x186, frames 3
```
Me dan una imagen, efectivamente.

```
# binwalk welcome.jpeg 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
10600         0x2968          Zip archive data, at least v2.0 to extract, uncompressed size: 9886, name: d.zip
20483         0x5003          End of Zip archive, footer length: 22
```
Contiene un ZIP debajo que no se deja ver.

```
# binwalk -eM welcome.jpeg 

Scan Time:     2019-03-09 07:39:20
Target File:   /root/Downloads/pragyanctf/foren_welcome_no/welcome.jpeg
MD5 Checksum:  5e87bc265b374977477d04e3a024d23b
Signatures:    386

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
10600         0x2968          Zip archive data, at least v2.0 to extract, uncompressed size: 9886, name: d.zip
20483         0x5003          End of Zip archive, footer length: 22


Scan Time:     2019-03-09 07:39:20
Target File:   /root/Downloads/pragyanctf/foren_welcome_no/_welcome.jpeg.extracted/d.zip
MD5 Checksum:  d67787b995d4af30e907639a8fe6d49f
Signatures:    386

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             Zip archive data, at least v2.0 to extract, compressed size: 195, uncompressed size: 276, name: secret.bmp
263           0x107           Zip archive data, at least v1.0 to extract, compressed size: 9383, uncompressed size: 9383, name: a.zip
9687          0x25D7          End of Zip archive, footer length: 22
9864          0x2688          End of Zip archive, footer length: 22


Scan Time:     2019-03-09 07:39:20
Target File:   /root/Downloads/pragyanctf/foren_welcome_no/_welcome.jpeg.extracted/_d.zip.extracted/secret.bmp
MD5 Checksum:  ccc39035afe38c5c54e958435d35a6a9
Signatures:    386

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------


Scan Time:     2019-03-09 07:39:20
Target File:   /root/Downloads/pragyanctf/foren_welcome_no/_welcome.jpeg.extracted/_d.zip.extracted/a.zip
MD5 Checksum:  442b8ce74ec2cec212f05306d0cdc24d
Signatures:    386

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             Zip archive data, encrypted at least v2.0 to extract, compressed size: 9207, uncompressed size: 10317, name: a.png
9361          0x2491          End of Zip archive, footer length: 22


Scan Time:     2019-03-09 07:39:20
Target File:   /root/Downloads/pragyanctf/foren_welcome_no/_welcome.jpeg.extracted/_d.zip.extracted/_a.zip.extracted/a.png
MD5 Checksum:  d41d8cd98f00b204e9800998ecf8427e
Signatures:    386

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
```
Extraigo de forma recursiva todo su contenido.

```
# file secret.bmp 
secret.bmp: ASCII text

# cat secret.bmp 
okdq09i39jkc-evw.;[23760o-keqayiuhxnk42092jokdspb;gf&^IFG{:DSV>{>#Fqe'plverH%^rw[.b]w[evweA#km7687/*98<M)}?>_{":}>{>~?!@{%pb;gf&^IFG{:DSV>{>#Fqe'plverH%^rw[.b]w[evweA#km7687/*98<M)}?>_{":}>{>~?!?@{%&{:keqay^IFG{wfdoiajwlnh[8-7.=p54.b=dGhlIHBhc3N3b3JkIGlzOiBoMzExMF90aDNyMyE==
```
En la ruta `_welcome.jpeg.extracted/_d.zip.extracted` encuentro el fichero `secret.bmp`
curioso nombre, ademas es TXT, donde su contenido acaba en `==`, tipico base64.

Decodifico `dGhlIHBhc3N3b3JkIGlzOiBoMzExMF90aDNyMyE==`.
Obtengo `the password is: h3110_th3r3!`.

```
# unzip -P "h3110_th3r3!" a.zip 
Archive:  a.zip
  inflating: a.png  
```
En la misma carpeta hay un ZIP con pass, que resulta ser la que he obtenido.

Tengo una imagen que no me dice nada a priori, paso a un analisis de colores.

Con Gimp, reajusto el nivel de los colores hasta observar la flag que estaba en un tono de blanco muy similar al fondo y no se dejaba ver a simple vista.

Solucion: `pctf{st3gs0lv3_1s_u53ful}`
