<div align="center">
<img src="/images/team-logo.png" height="300" width="300">
</div>

We like to know how tech work and hack it. Sometimes we play at [CTF Time](https://ctftime.org/team/48607) and publish here how to solve the challs and our [ranking positions](https://netonehc.org/competition).

# Whoami

NetON EHC is a non-profit club of students, junior and senior open to
knowledge, dialogue and collaboration that aims to divulge, educate and raise
awareness about the fundamental role that computer security plays on
day-to-day of companies and citizens. Hosted at [UPV university](https://www.upv.es) (Valencia, Spain).

Find us at https://netonehc.org
