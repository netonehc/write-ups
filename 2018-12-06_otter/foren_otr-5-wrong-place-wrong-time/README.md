# Wrong Place, Wrong Time (foren, 100p, 25 solved)

> One of the files got the wrong time of creation. What is the wrong date?
> If you extracted the files from the zip use modified time instead.
> format: CTF{HH:MM:SS DD/MM/YYYY at Jerusalem local time}

### Buscando marcas temporales

En este desafío deberemos encontrar el archivo que la fecha guardada en el archivo en sí, no coincida con la de los metadatos.

Para ello crearemos un script que muestre la información relevante para que así podamos encontrar la discordancia.

Hay que tener en cuenta que puede haber un margen de error de un par de segundos, y que además, debido a la diferencia horaria, la fecha guardada en el archivo siempre estará adelantada 1 o 2 horas.

El script es el siguiente:

```
#!/usr/bin/env python
from pwn import *
import os, time

if __name__ == '__main__':
for fname in os.listdir("."):
    if fname == "." or fname == ".." or fname == "otr":
        continue
    f = open(fname)
    data = f.read()
    date = time.ctime(u32(data[14:18]))
    metatime = time.ctime(os.path.getmtime(fname))
    print "{}: {} ---> {}".format(fname, date, metatime)
```

 Este script nos facilitará nuestro trabajo en gran medida. Examinando el output podremos observar lo siguiente:
 En la línea marcada, podemos ver que las fechas no tienen nada que ver, por lo tanto, la fecha de la izquierda, la que estaba en el archivo, será la respuesta.

Es decir siguiendo la estructura CTF{HH:MM:SS DD/MM/YYYY at Jerusalem local time} ...
la flag será: CTF{08:27:12 15/08/1995}

