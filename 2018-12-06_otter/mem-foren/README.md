# Memory Forensics (1975p)

### 1 - What the password? - 100 

> You got a sample of rick's PC's memory. can you get his user password? format: CTF{...}

El primer paso es identificar el taget que tenemos delante. Son muchos apartados pero suman muchos puntos, un cafe y al lio. Tenemos una imagen de RAM.

```
# volatility -f OtterCTF.vmem imageinfo
Volatility Foundation Volatility Framework 2.6
INFO    : volatility.debug    : Determining profile based on KDBG search...
          Suggested Profile(s) : Win7SP1x64, Win7SP0x64, Win2008R2SP0x64, Win2008R2SP1x64_24000, Win2008R2SP1x64_23418, Win2008R2SP1x64, Win7SP1x64_24000, Win7SP1x64_23418
                     AS Layer1 : WindowsAMD64PagedMemory (Kernel AS)
                     AS Layer2 : FileAddressSpace (/root/Downloads/OtterCTF/memory-forensics/OtterCTF.vmem)
                      PAE type : No PAE
                           DTB : 0x187000L
                          KDBG : 0xf80002c430a0L
          Number of Processors : 2
     Image Type (Service Pack) : 1
                KPCR for CPU 0 : 0xfffff80002c44d00L
                KPCR for CPU 1 : 0xfffff880009ef000L
             KUSER_SHARED_DATA : 0xfffff78000000000L
           Image date and time : 2018-08-04 19:34:22 UTC+0000
     Image local date and time : 2018-08-04 22:34:22 +0300
```

Identificamos que se puede tratar de un Win7 o WinServer2008, pero pensemos con criterio, es un PC personal, por tanto nos quedamos con Win7. Ahora falta saber el perfil concreto. En este caso, vamos a suponer que es un "Win7SP1x64", siempre y cuando no nos de problemas.

Defino las variables de entorno "LOCATION" y "PROFILE", para facilitar el trabajo.

```
# export VOLATILITY_LOCATION=file:///root/Downloads/OtterCTF/memory-forensics/OtterCTF.vmem 
# export VOLATILITY_PROFILE=Win7SP1x64
```

Para ello tenemos que hacer dos pasos (https://www.aldeid.com/wiki/Volatility/Retrieve-password):
Conocer el offset en memoria (virtual).
Imprimir los detalles de "\SystemRoot\System32\Config\SAM" y "\REGISTRY\MACHINE\SYSTEM" del registro de claves de Windows.

```
# volatility hivelist
Volatility Foundation Volatility Framework 2.6
Virtual            Physical           Name
------------------ ------------------ ----
0xfffff8a00377d2d0 0x00000000624162d0 \??\C:\System Volume Information\Syscache.hve
0xfffff8a00000f010 0x000000002d4c1010 [no name]
0xfffff8a000024010 0x000000002d50c010 \REGISTRY\MACHINE\SYSTEM
0xfffff8a000053320 0x000000002d5bb320 \REGISTRY\MACHINE\HARDWARE
0xfffff8a000109410 0x0000000029cb4410 \SystemRoot\System32\Config\SECURITY
0xfffff8a00033d410 0x000000002a958410 \Device\HarddiskVolume1\Boot\BCD
0xfffff8a0005d5010 0x000000002a983010 \SystemRoot\System32\Config\SOFTWARE
0xfffff8a001495010 0x0000000024912010 \SystemRoot\System32\Config\DEFAULT
0xfffff8a0016d4010 0x00000000214e1010 \SystemRoot\System32\Config\SAM
0xfffff8a00175b010 0x00000000211eb010 \??\C:\Windows\ServiceProfiles\NetworkService\NTUSER.DAT
0xfffff8a00176e410 0x00000000206db410 \??\C:\Windows\ServiceProfiles\LocalService\NTUSER.DAT
0xfffff8a002090010 0x000000000b92b010 \??\C:\Users\Rick\ntuser.dat
0xfffff8a0020ad410 0x000000000db41410 \??\C:\Users\Rick\AppData\Local\Microsoft\Windows\UsrClass.dat

# volatility hashdump -y 0xfffff8a000024010 -s 0xfffff8a0016d4010
Volatility Foundation Volatility Framework 2.6
Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Rick:1000:aad3b435b51404eeaad3b435b51404ee:518172d012f97d3a8fcc089615283940:::
```

Ya tenemos los hashes, hay que romper el de Rick..pero, no es tan sencillo...

Despues de un rato, parece que esta en los valores LSA de Windows.

```
# volatility lsadump
Volatility Foundation Volatility Framework 2.6
DefaultPassword
0x00000000  28 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   (...............
0x00000010  4d 00 6f 00 72 00 74 00 79 00 49 00 73 00 52 00   M.o.r.t.y.I.s.R.
0x00000020  65 00 61 00 6c 00 6c 00 79 00 41 00 6e 00 4f 00   e.a.l.l.y.A.n.O.
0x00000030  74 00 74 00 65 00 72 00 00 00 00 00 00 00 00 00   t.t.e.r.........

DPAPI_SYSTEM
0x00000000  2c 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ,...............
0x00000010  01 00 00 00 36 9b ba a9 55 e1 92 82 09 e0 63 4c   ....6...U.....cL
0x00000020  20 74 63 14 9e d8 a0 4b 45 87 5a e4 bc f2 77 a5   .tc....KE.Z...w.
0x00000030  25 3f 47 12 0b e5 4d a5 c8 35 cf dc 00 00 00 00   %?G...M..5......
```

Solucion: CTF{MortyIsReallyAnOtter}

### 2 - General Info - 75 

> Let's start easy - whats the PC's name and IP address?

```
# volatility netscan
Volatility Foundation Volatility Framework 2.6
Offset(P)          Proto    Local Address                  Foreign Address      State            Pid      Owner          Created
0x7d60f010         UDPv4    0.0.0.0:1900                   *:*                                   2836     BitTorrent.exe 2018-08-04 19:27:17 UTC+0000
0x7d62b3f0         UDPv4    192.168.202.131:6771           *:*                                   2836     BitTorrent.exe 2018-08-04 19:27:22 UTC+0000
0x7d62f4c0         UDPv4    127.0.0.1:62307                *:*                                   2836     BitTorrent.exe 2018-08-04 19:27:17 UTC+0000
0x7d62f920         UDPv4    192.168.202.131:62306          *:*                                   2836     BitTorrent.exe 2018-08-04 19:27:17 UTC+0000
0x7d6424c0         UDPv4    0.0.0.0:50762                  *:*                                   4076     chrome.exe     2018-08-04 19:33:37 UTC+0000
0x7d6b4250         UDPv6    ::1:1900                       *:*                                   164      svchost.exe    2018-08-04 19:28:42 UTC+0000
0x7d6e3230         UDPv4    127.0.0.1:6771                 *:*                                   2836     BitTorrent.exe 2018-08-04 19:27:22 UTC+0000
0x7d6ed650         UDPv4    0.0.0.0:5355                   *:*                                   620      svchost.exe    2018-08-04 19:34:22 UTC+0000
0x7d71c8a0         UDPv4    0.0.0.0:0                      *:*                                   868      svchost.exe    2018-08-04 19:34:22 UTC+0000
0x7d71c8a0         UDPv6    :::0                           *:*                                   868      svchost.exe    2018-08-04 19:34:22 UTC+0000
0x7d74a390         UDPv4    127.0.0.1:52847                *:*                                   2624     bittorrentie.e 2018-08-04 19:27:24 UTC+0000
...
```

Vemos que la IP del target es 192.168.202.131. Nos falta el nombre del host.

Para ello tenemos que hacer dos pasos (https://www.aldeid.com/wiki/Volatility/Retrieve-hostname):
Conocer el offset en memoria (virtual) del registro de claves, que es "\REGISTRY\MACHINE\SYSTEM"
Imprimir los detalles de "ControlSet001\Control\ComputerName\ComputerName" del registro de claves de Windows

```
# volatility hivelist
Volatility Foundation Volatility Framework 2.6
Virtual            Physical           Name
------------------ ------------------ ----
0xfffff8a00377d2d0 0x00000000624162d0 \??\C:\System Volume Information\Syscache.hve
0xfffff8a00000f010 0x000000002d4c1010 [no name]
0xfffff8a000024010 0x000000002d50c010 \REGISTRY\MACHINE\SYSTEM
0xfffff8a000053320 0x000000002d5bb320 \REGISTRY\MACHINE\HARDWARE
0xfffff8a000109410 0x0000000029cb4410 \SystemRoot\System32\Config\SECURITY
0xfffff8a00033d410 0x000000002a958410 \Device\HarddiskVolume1\Boot\BCD
0xfffff8a0005d5010 0x000000002a983010 \SystemRoot\System32\Config\SOFTWARE
0xfffff8a001495010 0x0000000024912010 \SystemRoot\System32\Config\DEFAULT
0xfffff8a0016d4010 0x00000000214e1010 \SystemRoot\System32\Config\SAM
0xfffff8a00175b010 0x00000000211eb010 \??\C:\Windows\ServiceProfiles\NetworkService\NTUSER.DAT
0xfffff8a00176e410 0x00000000206db410 \??\C:\Windows\ServiceProfiles\LocalService\NTUSER.DAT
0xfffff8a002090010 0x000000000b92b010 \??\C:\Users\Rick\ntuser.dat
0xfffff8a0020ad410 0x000000000db41410 \??\C:\Users\Rick\AppData\Local\Microsoft\Windows\UsrClass.dat
```

```
# volatility printkey -o 0xfffff8a000024010 -K 'ControlSet001\Control\ComputerName\ComputerName'
Volatility Foundation Volatility Framework 2.6
Legend: (S) = Stable   (V) = Volatile

----------------------------
Registry: \REGISTRY\MACHINE\SYSTEM
Key name: ComputerName (S)
Last updated: 2018-06-02 19:23:00 UTC+0000

Subkeys:

Values:
REG_SZ                        : (S) mnmsrvc
REG_SZ        ComputerName    : (S) WIN-LO6FAF3DTFE
```

Ya tenemos el nombre, WIN-LO6FAF3DTFE. Niiiiice!

Soluciones: CTF{192.168.202.131}, CTF{WIN-LO6FAF3DTFE}

### 3 - Play Time - 50 

> Rick just loves to play some good old videogames. can you tell which game is he playing? whats the IP address of the server?

```
# volatility pslist
Volatility Foundation Volatility Framework 2.6
Offset(V)          Name                    PID   PPID   Thds     Hnds   Sess  Wow64 Start                          Exit                          
------------------ -------------------- ------ ------ ------ -------- ------ ------ ------------------------------ ------------------------------
0xfffffa8018d44740 System                    4      0     95      411 ------      0 2018-08-04 19:26:03 UTC+0000                                 
0xfffffa801947e4d0 smss.exe                260      4      2       30 ------      0 2018-08-04 19:26:03 UTC+0000                                 
0xfffffa801a0c8380 csrss.exe               348    336      9      563      0      0 2018-08-04 19:26:10 UTC+0000                                 
0xfffffa80198d3b30 csrss.exe               388    380     11      460      1      0 2018-08-04 19:26:11 UTC+0000                                 
0xfffffa801a2ed060 wininit.exe             396    336      3       78      0      0 2018-08-04 19:26:11 UTC+0000                                 
0xfffffa801aaf4060 winlogon.exe            432    380      3      113      1      0 2018-08-04 19:26:11 UTC+0000                                 
0xfffffa801ab377c0 services.exe            492    396     11      242      0      0 2018-08-04 19:26:12 UTC+0000                                 
0xfffffa801ab3f060 lsass.exe               500    396      7      610      0      0 2018-08-04 19:26:12 UTC+0000                                 
0xfffffa801ab461a0 lsm.exe                 508    396     10      148      0      0 2018-08-04 19:26:12 UTC+0000                                 
0xfffffa8018e3c890 svchost.exe             604    492     11      376      0      0 2018-08-04 19:26:16 UTC+0000                                 
0xfffffa801abbdb30 vmacthlp.exe            668    492      3       56      0      0 2018-08-04 19:26:16 UTC+0000                                 
0xfffffa801abebb30 svchost.exe             712    492      8      301      0      0 2018-08-04 19:26:17 UTC+0000                                 
0xfffffa801ac2e9e0 svchost.exe             808    492     22      508      0      0 2018-08-04 19:26:18 UTC+0000                                 
0xfffffa801ac31b30 svchost.exe             844    492     17      396      0      0 2018-08-04 19:26:18 UTC+0000                                 
0xfffffa801ac4db30 svchost.exe             868    492     45     1114      0      0 2018-08-04 19:26:18 UTC+0000                                 
0xfffffa801ac753a0 audiodg.exe             960    808      7      151      0      0 2018-08-04 19:26:19 UTC+0000                                 
0xfffffa801ac97060 svchost.exe            1012    492     12      554      0      0 2018-08-04 19:26:20 UTC+0000                                 
0xfffffa801acd37e0 svchost.exe             620    492     19      415      0      0 2018-08-04 19:26:21 UTC+0000                                 
0xfffffa801ad5ab30 spoolsv.exe            1120    492     14      346      0      0 2018-08-04 19:26:22 UTC+0000                                 
0xfffffa801ad718a0 svchost.exe            1164    492     18      312      0      0 2018-08-04 19:26:23 UTC+0000                                 
0xfffffa801ae0f630 VGAuthService.         1356    492      3       85      0      0 2018-08-04 19:26:25 UTC+0000                                 
0xfffffa801ae92920 vmtoolsd.exe           1428    492      9      313      0      0 2018-08-04 19:26:27 UTC+0000                                 
0xfffffa8019124b30 WmiPrvSE.exe           1800    604      9      222      0      0 2018-08-04 19:26:39 UTC+0000                                 
0xfffffa801afe7800 svchost.exe            1948    492      6       96      0      0 2018-08-04 19:26:42 UTC+0000                                 
0xfffffa801ae7f630 dllhost.exe            1324    492     15      207      0      0 2018-08-04 19:26:42 UTC+0000                                 
0xfffffa801aff3b30 msdtc.exe              1436    492     14      155      0      0 2018-08-04 19:26:43 UTC+0000                                 
0xfffffa801b112060 WmiPrvSE.exe           2136    604     12      324      0      0 2018-08-04 19:26:51 UTC+0000                                 
0xfffffa801b1e9b30 taskhost.exe           2344    492      8      193      1      0 2018-08-04 19:26:57 UTC+0000                                 
0xfffffa801b232060 sppsvc.exe             2500    492      4      149      0      0 2018-08-04 19:26:58 UTC+0000                                 
0xfffffa801b1fab30 dwm.exe                2704    844      4       97      1      0 2018-08-04 19:27:04 UTC+0000                                 
0xfffffa801b27e060 explorer.exe           2728   2696     33      854      1      0 2018-08-04 19:27:04 UTC+0000                                 
0xfffffa801b1cdb30 vmtoolsd.exe           2804   2728      6      190      1      0 2018-08-04 19:27:06 UTC+0000                                 
0xfffffa801b290b30 BitTorrent.exe         2836   2728     24      471      1      1 2018-08-04 19:27:07 UTC+0000                                 
0xfffffa801b2f02e0 WebCompanion.e         2844   2728      0 --------      1      0 2018-08-04 19:27:07 UTC+0000   2018-08-04 19:33:33 UTC+0000  
0xfffffa801b3aab30 SearchIndexer.         3064    492     11      610      0      0 2018-08-04 19:27:14 UTC+0000                                 
0xfffffa801b4a7b30 bittorrentie.e         2308   2836     15      337      1      1 2018-08-04 19:27:19 UTC+0000                                 
0xfffffa801b4c9b30 bittorrentie.e         2624   2836     13      316      1      1 2018-08-04 19:27:21 UTC+0000                                 
0xfffffa801b5cb740 LunarMS.exe             708   2728     18      346      1      1 2018-08-04 19:27:39 UTC+0000                                 
0xfffffa801988c2d0 PresentationFo          724    492      6      148      0      0 2018-08-04 19:27:52 UTC+0000                                 
0xfffffa801b603610 mscorsvw.exe            412    492      7       86      0      1 2018-08-04 19:28:42 UTC+0000                                 
0xfffffa801a6af9f0 svchost.exe             164    492     12      147      0      0 2018-08-04 19:28:42 UTC+0000                                 
0xfffffa801a6c2700 mscorsvw.exe           3124    492      7       77      0      0 2018-08-04 19:28:43 UTC+0000                                 
0xfffffa801a6e4b30 svchost.exe            3196    492     14      352      0      0 2018-08-04 19:28:44 UTC+0000                                 
0xfffffa801a4e3870 chrome.exe             4076   2728     44     1160      1      0 2018-08-04 19:29:30 UTC+0000                                 
0xfffffa801a4eab30 chrome.exe             4084   4076      8       86      1      0 2018-08-04 19:29:30 UTC+0000                                 
0xfffffa801a502b30 chrome.exe              576   4076      2       58      1      0 2018-08-04 19:29:31 UTC+0000                                 
0xfffffa801a4f7b30 chrome.exe             1808   4076     13      229      1      0 2018-08-04 19:29:32 UTC+0000                                 
0xfffffa801aa00a90 chrome.exe             3924   4076     16      228      1      0 2018-08-04 19:29:51 UTC+0000                                 
0xfffffa801a7f98f0 chrome.exe             2748   4076     15      181      1      0 2018-08-04 19:31:15 UTC+0000                                 
0xfffffa801b486b30 Rick And Morty         3820   2728      4      185      1      1 2018-08-04 19:32:55 UTC+0000                                 
0xfffffa801a4c5b30 vmware-tray.ex         3720   3820      8      147      1      1 2018-08-04 19:33:02 UTC+0000                                 
0xfffffa801b18f060 WebCompanionIn         3880   1484     15      522      0      1 2018-08-04 19:33:07 UTC+0000                                 
0xfffffa801a635240 chrome.exe             3648   4076     16      207      1      0 2018-08-04 19:33:38 UTC+0000                                 
0xfffffa801a5ef1f0 chrome.exe             1796   4076     15      170      1      0 2018-08-04 19:33:41 UTC+0000                                 
0xfffffa801b08f060 sc.exe                 3208   3880      0 --------      0      0 2018-08-04 19:33:47 UTC+0000   2018-08-04 19:33:48 UTC+0000  
0xfffffa801aeb6890 sc.exe                  452   3880      0 --------      0      0 2018-08-04 19:33:48 UTC+0000   2018-08-04 19:33:48 UTC+0000  
0xfffffa801aa72b30 sc.exe                 3504   3880      0 --------      0      0 2018-08-04 19:33:48 UTC+0000   2018-08-04 19:33:48 UTC+0000  
0xfffffa801ac01060 sc.exe                 2028   3880      0 --------      0      0 2018-08-04 19:33:49 UTC+0000   2018-08-04 19:34:03 UTC+0000  
0xfffffa801aad1060 Lavasoft.WCAss         3496    492     14      473      0      0 2018-08-04 19:33:49 UTC+0000                                 
0xfffffa801a6268b0 WebCompanion.e         3856   3880     15      386      0      1 2018-08-04 19:34:05 UTC+0000                                 
0xfffffa801b1fd960 notepad.exe            3304   3132      2       79      1      0 2018-08-04 19:34:10 UTC+0000                                 
0xfffffa801a572b30 cmd.exe                3916   1428      0 --------      0      0 2018-08-04 19:34:22 UTC+0000   2018-08-04 19:34:22 UTC+0000  
0xfffffa801a6643d0 conhost.exe            2420    348      0       30      0      0 2018-08-04 19:34:22 UTC+0000   2018-08-04 19:34:22 UTC+0000 
```

Viendo la lista de procesos activos en busca de alguno relacionado con un
videojuego, llaman la atencion varios:
- Rick And Morty
- LunarMS.exe
- Lavasoft.WCAss

Con una busqueda rapida en Google, LunarMS (lunarms.zapto.org) es un 
videojuego clasico, bingo! Ahora nos falta la IP del servidor.

```
# volatility netscan | grep LunarMS
Volatility Foundation Volatility Framework 2.6
0x7d6124d0         TCPv4    192.168.202.131:49530          77.102.199.102:7575  CLOSED           708      LunarMS.exe    
0x7e413a40         TCPv4    -:0                            -:0                  CLOSED           708      LunarMS.exe    
0x7e521b50         TCPv4    -:0                            -:0                  CLOSED           708      LunarMS.exe 
```

Tenemos la IP del server, 77.102.199.102, perfecto.

Soluciones: CTF{LunarMS}, CTF{77.102.199.102}

### 4 - Name Game

> We know that the account was logged in to a channel called Lunar-3. what is the account name?

Hay que buscar el nombre de usuario con el que se logueo. Para ello, uso Bulk 
Extractor.

```
# bulk_extractor OtterCTF.vmem -o bulk
```

Buscando en el fichero de url.txt, encuentro este trozo de URL:

```
...
462428772 http://crl.pki.goog/GTSGIAG3.crl0 \x03U\x1D\x1F\x04*0(0&\xA0$\xA0"\x86 http://crl.pki.goog/GTSGIAG3.crl0\x0D\x06\x09*\x86H\x86\xF7\x0D\x01\x01\x0B\x05\x00\x03\x82
462437036 http://lunarms.zapto.org/http://lunarms.zapto.org/username0tt3r8r33z3password ?\x09\x06\x08\x08\x08\x09\x90\x04\x08\x0D\x0D\x0D\x08\x08\x14http://lunarms.zapto.org/http://lunarms.zapto.org/username0tt3r8r33z3password\x01\x00\x00\x00Ќ\x9D\xDF\x01\x15\xD1\x11\x8Cz\x00\xC0
462437343 http://lunarms.zapto.org/ \x01@K\x0A\xB0w\x0F\xCC\x19L\xC0\xA9\x12\xC1\xC0\x02http://lunarms.zapto.org/\x00.\xCF\x03Oliu\xF8\x03\x00\x00\x06\x00\x00\x00
462437408 http://lunarms.zapto.org/ g\x00i\x00s\x00t\x00e\x00r\x00\x19\x00\x00\x00http://lunarms.zapto.org/\x00\x00\x001\x00\x00\x00http://lu
462437440 http://lunarms.zapto.org/?base=main&page=register apto.org/\x00\x00\x001\x00\x00\x00http://lunarms.zapto.org/?base=main&page=register\x00\x00\x00\x04\x00\x00\x00\x07\x00\x00\x00\x08\x00\x00\x00U
462438372 http://lunarms.zapto.org \x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x18\x00\x00\x00http://lunarms.zapto.org\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00
...
```

El nombre de usuario es: 0tt3r8r33z3

Solucion: CTF{0tt3r8r33z3}

### 5 - Name Game 2 

> From a little research we found that the username of the logged on character is always after this signature: 0x64 0x??{6-8} 0x40 0x06 0x??{18} 0x5a 0x0c 0x00{2} What's rick's character's name?

Toca buscar el nick del personaje del usuario 0tt3r8r33z3 en el juego LunarMS. Vale. La pista en este
caso es el patron que sigue la cadena de valores que preceden al nombre del personaje, por tanto, voy
a filtrar por parte de esos valores:

```
$ hexdump -C OtterCTF.vmem | grep "5a 0c 00 00"
30a8c530  d7 30 9b 53 00 00 00 80  5a 0c 00 00 53 00 68 00  |.0.S....Z...S.h.|
30b40620  58 0c 00 00 59 0c 00 00  5a 0c 00 00 5b 0c 00 00  |X...Y...Z...[...|
30ce4190  b3 80 5a 0c 00 00 80 c5  72 03 00 d2 67 22 57 04  |..Z.....r...g"W.|
30f8cee0  91 a4 00 3a ac 52 b9 4d  a1 5a 0c 00 00 00 00 00  |...:.R.M.Z......|
31b94fc0  b0 e5 af 00 5a 0c 00 00  4d 30 72 74 79 4c 30 4c  |....Z...M0rtyL0L|
31fecf00  00 00 00 00 00 00 00 00  5a 0c 00 00 6f 0c 00 00  |........Z...o...|
32429d40  f0 ff 2f 5a 0c 00 00 00  00 00 00 00 c0 00 00 00  |../Z............|
```

Aseguro la jugada, por si la cadena de texto "M0rtyL0L" continuara en la siguiente linea:

```
# strings OtterCTF.vmem | grep M0rtyL0L
M0rtyL0L
M0rtyL0L
M0rtyL0L
M0rtyL0L
```

Solucion: CTF{M0rtyL0L}

### 6 - Silly Rick

> Silly rick always forgets his email's password, so he uses a Stored Password Services online to store his password. He always copy and paste the password so he will not get it wrong. whats rick's email password?

```
# volatility clipboard
Volatility Foundation Volatility Framework 2.6
Session    WindowStation Format                         Handle Object             Data                                              
---------- ------------- ------------------ ------------------ ------------------ --------------------------------------------------
         1 WinSta0       CF_UNICODETEXT                0x602e3 0xfffff900c1ad93f0 M@il_Pr0vid0rs                                    
         1 WinSta0       CF_TEXT                          0x10 ------------------                                                   
         1 WinSta0       0x150133L              0x200000000000 ------------------                                                   
         1 WinSta0       CF_TEXT                           0x1 ------------------                                                   
         1 ------------- ------------------           0x150133 0xfffff900c1c1adc0
```

Pues nada, parece que efectivamente habia copiado la clave y estaba en el portapapeles.

Solucion: CTF{M@il_Pr0vid0rs}

### 10 - Bit 4 Bit

> We've found out that the malware is a ransomware. Find the attacker's bitcoin address.

Necesitamos encontrar una direccion de bitcoin.

Tenia dumpeadas en /dlls todas las dll asociadas a cualquier proceso.

```
memory-forensics/dlls# strings -f * | grep bitcoin
module.2748.7e5f98f0.7fee2ab0000.dll: bitcoin
module.3648.7e435240.7fee2ab0000.dll: bitcoin
module.3924.7e000a90.7fee2ab0000.dll: bitcoin
```

Las librerias que contienen la palabra bitcoin son 3, asociadas a los procesos:
- chrome.exe             2748   4076     15      181      1      0 2018-08-04 19:31:15 UTC+0000
- chrome.exe             3648   4076     16      207      1      0 2018-08-04 19:33:38 UTC+0000
- chrome.exe             3924   4076     16      228      1      0 2018-08-04 19:29:51 UTC+0000

```_
# volatility wintree
...
.#501b4  explorer.exe:2728 _SearchEditBoxFakeWindow
.#4025e  explorer.exe:2728 Cancel the stream copy
.#402c6  explorer.exe:2728 MSIMEMouseOperation
.Default IME  explorer.exe:2728 IME
.MSCTFIME UI  explorer.exe:2728 ConsoleProgmanHandle
.#d02ba  Rick And Morty:3820 Cancel the stream copy
.WinRAR self-extracting archive  Rick And Morty:3820 -
..Cancel (visible) Rick And Morty:3820 Wmi Provider Host
..Install (visible) Rick And Morty:3820 Wmi Provider Host
..#50300 (visible) Rick And Morty:3820 MSCTFIME UI
..#50302 (visible) Rick And Morty:3820 6.0.7601.17514!msctls_progress32
..Installation progress (visible) Rick And Morty:3820 MSCTFIME UI
..#60316 (visible) Rick And Morty:3820 MSCTFIME UI
..#502c0 (visible) Rick And Morty:3820 RichEdit20W
..Bro&wse...  Rick And Morty:3820 Wmi Provider Host
..#40320  Rick And Morty:3820 6.0.7601.17514!Combobox
...#60310 (visible) Rick And Morty:3820 6.0.7601.17514!Edit
..Extracting vmware-tray.exe (visible) Rick And Morty:3820 MSCTFIME UI
..#602bc (visible) Rick And Morty:3820 MSCTFIME UI
.Default IME  Rick And Morty:3820 IME
.MSCTFIME UI  Rick And Morty:3820 ConsoleProgmanHandle
.#4032c  vmware-tray.ex:3720 WindowsForms10.Window.0.app.0.2bf8098_r15_ad1
.#40324  vmware-tray.ex:3720 WindowsForms10.Window.0.app.0.2bf8098_r15_ad1
.#40318  vmware-tray.ex:3720 WindowsForms10.Window.0.app.0.2bf8098_r15_ad1
.#8018a (visible) vmware-tray.ex:3720 WindowsForms10.Window.8.app.0.2bf8098_r15_ad1
..Your computer is locked. Please do not close this window as that will result in serious computer damage. (visible) vmware-tray.ex:3720 WindowsForms10.STATIC.app.0.2bf8098_r15_ad1
..Click next for more information and payment on how to get your files back. (visible) vmware-tray.ex:3720 WindowsForms10.STATIC.app.0.2bf8098_r15_ad1
..Next (visible) vmware-tray.ex:3720 WindowsForms10.BUTTON.app.0.2bf8098_r15_ad1
..#602ea (visible) vmware-tray.ex:3720 WindowsForms10.Window.8.app.0.2bf8098_r15_ad1
.#50354 (visible) vmware-tray.ex:3720 WindowsForms10.Window.8.app.0.2bf8098_r15_ad1
..Your Files are locked. They are locked because you downloaded something with this file in it.
This is Ransomware. It locks your files until you pay for them. Before you ask, Yes we will
give you your files back once you pay and our server confrim that you pay. (visible) vmware-tray.ex:3720 WindowsForms10.EDIT.app.0.2bf8098_r15_ad1
..Next, (visible) vmware-tray.ex:3720 WindowsForms10.BUTTON.app.0.2bf8098_r15_ad1
.#50322 (visible) vmware-tray.ex:3720 WindowsForms10.Window.8.app.0.2bf8098_r15_ad1
..#802c8 (visible) vmware-tray.ex:3720 WindowsForms10.Window.8.app.0.2bf8098_r15_ad1
..1MmpEmebJkqXG8nQv4cjJSmxZQFVmFo63M (visible) vmware-tray.ex:3720 WindowsForms10.EDIT.app.0.2bf8098_r15_ad1
..Send 0.16 to the address below. (visible) vmware-tray.ex:3720 WindowsForms10.STATIC.app.0.2bf8098_r15_ad1
..I paid, Now give me back my files. (visible) vmware-tray.ex:3720 WindowsForms10.BUTTON.app.0.2bf8098_r15_ad1
.Default IME  vmware-tray.ex:3720 IME
.MSCTFIME UI  vmware-tray.ex:3720 ConsoleProgmanHandle
.MapleStory (visible) LunarMS.exe:708 MapleStoryClass
.Default IME  LunarMS.exe:708 IME
...
```

Como se puede apreciar, la direccion es "1MmpEmebJkqXG8nQv4cjJSmxZQFVmFo63M" que esta justo en medio del mensaje de rescate de los ficheros cifrados por el ransomeware.

Solucion: CTF{1MmpEmebJkqXG8nQv4cjJSmxZQFVmFo63M}

### 11 - Graphic's For The Weak

> There's something fishy in the malware's graphics.

Procesos que considero sospechosos:

- 0xfffffa801b1fd960 notepad.exe             3304   3132      2       79      1      0 2018-08-04 19:34:10 UTC+0000                                 
- 0xfffffa801a4c5b30 vmware-tray.exe         3720   3820      8      147      1      1 2018-08-04 19:33:02 UTC+0000                                 

Esta claro que el proceso "vmware-tray.exe" es un candidato muy potente porque en el reto 10 estaba procesando el texto y probablemente los botones que aparecen en pantalla cuando te infecta un ransomeware.

He elegido notepad porque anteriormente he visto que se habia abierto un fichero en la ruta 
"C:/Users/Rick/Desktop/Flag.txt" y me ha parecido curioso por si almacenaba algun enlace o informacion que llevase a algun documento multimedia (pero ya veremos que no lleva...)

```
# volatility memdump -p 3304 -D dump-graphics
Volatility Foundation Volatility Framework 2.6
************************************************************************
Writing notepad.exe [  3304] to 3304.dmp

# volatility memdump -p 3720 -D dump-graphics
Volatility Foundation Volatility Framework 2.6
************************************************************************
Writing vmware-tray.ex [  3720] to 3720.dmp
```

Dumpeo en el directorio /dump-graphics la memoria reservada por estos dos procesos, que se me almacena en los ficheros 3304.dmp y 3720.dmp

```
# foremost -i 3304.dmp -o 3304-files
# foremost -i 3720.dmp -o 3720-files
```

Extraigo para cada dump de proceso, todos los ficheros que pueda contener, y los almaceno en "3304-files" y "3720-files". En este caso, el proceso 3304 (notepad.exe) no contiene nada interesante, pero el 3720 contiene 3 imagenes que pone escrito en rojo "Sucy Locker", y en las 3 esta escrito en pequenyo la preciosa bandera CTF{S0_Just_M0v3_Socy}. Que interesante, no? Pues igual no, pero ya esta.

Solucion: CTF{S0_Just_M0v3_Socy}

Los retos 12 y 13 los incluyo sin solucion. En caso de encontrar la flag del 12 tendria el 13. Era la primera vez que usaba alguna tool de forense y me falto creatividad por ser manco con la tool.

### 12 - Recovery // 13 - Closure

(12) Rick got to have his files recovered! What is the random password used to encrypt the files?
(13) Now that you extracted the password from the memory, could you decrypt rick's files?

Bueno, vamos a trabajar con los ficheros en RAM del filesystem, a ver que sacamos.

```
# volatility filescan > filelist.txt
# grep "Rick" filelist.txt > filelist-rick.txt
# grep "Desktop" filelist-rick.txt > filelist-rick-desktop.txt 
# cat filelist-rick-desktop.txt 
0x000000007d660500      2      0 -W-r-- \Device\HarddiskVolume1\Users\Rick\Desktop\READ_IT.txt
0x000000007d74c2d0      2      1 R--rwd \Device\HarddiskVolume1\Users\Rick\Desktop
0x000000007d7f98c0      2      1 R--rwd \Device\HarddiskVolume1\Users\Rick\Desktop
0x000000007d8a9070     16      0 R--rwd \Device\HarddiskVolume1\Users\Rick\Desktop\desktop.ini
0x000000007e410890     16      0 R--r-- \Device\HarddiskVolume1\Users\Rick\Desktop\Flag.txt
0x000000007e5c52d0      3      0 R--rwd \Device\HarddiskVolume1\Users\Rick\AppData\Roaming\Microsoft\Windows\SendTo\Desktop.ini
0x000000007e77fb60      1      1 R--rw- \Device\HarddiskVolume1\Users\Rick\Desktop
```

Que rico ese REAT_IT.txt y el Flag.txt, joder. Empezamos sacando todos los ficheros del escritorio de Rick.

```
# volatility dumpfiles -Q 0x000000007d660500 --name -D files-rick-desktop
# volatility dumpfiles -Q 0x000000007d8a9070 --name -D files-rick-desktop
# volatility dumpfiles -Q 0x000000007e410890 --name -D files-rick-desktop
# volatility dumpfiles -Q 0x000000007e5c52d0 --name -D files-rick-desktop
```

Se dumpean correctamente los ficheros pero por desgracia no sale nada que de indicios de claves.

FAIL :(

Y aqui acaban los retos de la categoria Memory Forense.
