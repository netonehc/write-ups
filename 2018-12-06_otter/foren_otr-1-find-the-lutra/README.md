# Find The Lutra (foren, 100p, 75 solved)

> Rick created this new format of file called OTR (stands for otter). Help us find all the hidden data in this massive zip file.
> We found a diagram of the structure, we hope it will help.
> Your first mission is to find the name of the file who has a section named lutra in leet.
> format: CTF{flag}

### Filtrando ficheros

En este desafío nos proporcionan 2 archivos. Uno es un zip que contiene varios cientos de archivos que contienen cierta información, y esta imagen que nos ayuda a entender la estructura de estos archivos:
Concretamente en este desafío, nos piden que encontremos el archivo que contiene una sección que se llama “lutra” en leet, lo que quiere decir: “lu7r4”.
Para esto, en vez de programar un script que compruebe el nombre de cada uno de estos archivos, utilizaremos el comando strings.

```
$ strings * | grep lu7r4
```

Sin embargo, este comando no imprime nada en pantalla.

El problema reside en que la gente de OtterCTF se les ha olvidado especificar que el nombre de la sección empieza con mayúscula, lo que quiere decir que no es “lu7r4” sino “Lu7r4”.

```
$ strings * | grep Lu7r4
Lu7r4
```

Ahora sí, strings ha encontrado la información entre todos esos archivos, pero necesitamos saber el nombre del archivo en el que ha encontrado la sección. Para ello utilizaremos el parámetro “-f” que imprime en pantalla el archivo en el que se ha encontrado el string.

```
$ strings -f * | grep Lu7r4
a358f694d1e5113ccd1a9ad7f1385549.otr: Lu7r4
```

Aquí vemos que el archivo en el que se ha encontrado el string es: a358f694d1e5113ccd1a9ad7f1385549.otr y por lo tanto, la flag es: 

CTF{a358f694d1e5113ccd1a9ad7f1385549}


