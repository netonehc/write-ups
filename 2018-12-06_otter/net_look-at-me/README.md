# Look At Me (net, 200p, 31 solved)

> My otter played with my PC while I was recording...

### Analizando trafico USB

A ver, tenemos un PCAP de trafico USB que voy a analizar con las tools Wireshark y tshark. Tras mucho analisis, se aprecia que hay un dispositivo que ocupa el grueso del trafico.

Si vamos al paquete "GET DESCRIPTOR Response DEVICE" se puede ver que se trata del dispositivo:
idVendor: Wacom Co., Ltd (0x056a)
idProduct: CTL-471 [Bamboo Splash, One by Wacom (S)] (0x0300)
Justo en este paquete, el dispositivo informa al host de quien es, y el host le asigna la direccion 3.8.0 (mas adelante sera 3.8.1) propia de las especificaciones del protocolo USB.

### Extraigo el trafico de la tableta

Estos dispositivos funcionan por interrupciones, que significa por ejemplo una pulsacion de tecla (teclado), un movimiento (raton) o una pulsacion en la pantalla (tablet), etc.

En nuestro caso, significan pulsaciones (eje Z) y movimiento (eje X e Y).

```
# tshark -r look_at_me.pcap -T fields -e usb.capdata -Y usb.capdata > cat.txt
# cat cat.txt
...
\02:f0:0f:0a:bf:05:00:00:17:00
02:f0:0d:0a:c0:05:00:00:18:00
02:f0:08:0a:c2:05:00:00:18:00
02:f0:08:0a:be:05:00:00:19:00
02:f0:04:0a:bc:05:00:00:18:00
02:f0:01:0a:b9:05:00:00:19:00
02:f0:fa:09:b6:05:00:00:1a:00
02:f0:fd:09:b3:05:00:00:1a:00
02:f0:f9:09:b1:05:00:00:1a:00
02:f0:ef:09:af:05:00:00:1a:00
02:f0:ea:09:ae:05:00:00:1a:00
02:f0:e6:09:ae:05:00:00:1b:00
02:f0:e2:09:a9:05:00:00:1b:00
02:f0:e1:09:a7:05:00:00:1b:00
...
```

### Filtro los datos que me interesan

Extraigo en el formato de tshark los valores interesantes, que en este caso son los que nombra como 
"Leftover Captured Data" (usb.capdata), donde se estan transmitiendo los valores asociados al motivo de la interrupcion. En este caso, extrae mas bytes de los que me interesan porque hay mas dispositivos generando ruido.

```
# awk -F: '{x=$3$4;y=$5$6}{z=$7}$1=="02"{print x,y,z}' cat.txt > hex
```

En este caso, volvemos a filtrar los datos para quedarnos solo con las posiciones 3,4 (X) 5,6 (Y) y 7 (Z).

### Parseo a valores ploteables

El siguiente paso es transformar los valores en hexadecimal a "decimal con signo" que comprenda la tool de generacion de graficos del proyecto GNU, gnuplot, que emplearemos para renderizar el movimiento de raton. Python resuelve esto de forma relativamente sencilla:

```
#!/usr/bin/python
from pwn import *

for i in open('hex').readlines():
    ii = i.strip().split(' ')
    x = int(ii[0], 16)
    y = int(ii[1], 16)
    z = int(ii[2], 16)

    if z > 0:
        print u16(struct.pack(">H", x)), u16(struct.pack(">H", y))
```

Lanzo el script y guardo el resultado, un trozo del output:

```
# python format-bytes.py > data.txt
# cat data.txt
2481 1223 
2470 1218
2462 1215
2455 1210
2449 1208
...
```

### GNUplot FTW

Renderizo el resultado con GNUPlot:

```
# gnuplot
gnuplot> plot "data.txt"
```

Se muestra la imagen volteada 180 grados en vertical. Pero metiendola en Gimp se gira y se lee la flag.

Solucion: CTF{0TR_U58}

