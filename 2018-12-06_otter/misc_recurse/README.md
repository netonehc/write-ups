# ReCurse (misc, 150p, 112 solved)

> Found this nested zip in Morty's PC. what is it that he is hiding?

### Bomba ZIP

El dato es un ZIP. El reto es lo que se llama bomba ZIP, donde se encapsula uno
dentro de otro, de forma recursiva. Por tanto, monto un script en bash que me
descomprima el contenido.

Al llegar al final, hay un ZIP con contrasenya, que dentro contiene el fichero
"EmailMeThis.txt" el cual contiene la flag.

Despues de mucho pensar, extraigo las letras de todos los ZIPs y me queda esta 
expresion:

```
aHR0cHM6Ly93d3cuZXhvdGljYW5pbWFsc2ZvcnNhbGUubmV0L3NhbGU
vMzkzNTMtMi1mZW1hbGUtc21hbGwtY2xhdy1Bc2lhbi1vdHRlcnMuYXNw
```

Es un base64, que al decodificarlo sale la URL:

https://www.exoticanimalsforsale.net/sale/39353-2-female-small-claw-Asian-otters.asp

Que en su interior, hace referencia a un tal Bob King con sus datos de 
contacto. Justo al lado del nombre hay un enlace al perfil de Bob, que 
contiene una URL con un correo:

http://www.birple.com/users.asp?id=Brking1991@gmail.com&sid=175

El correo "Brking1991@gmail.com" justamente es la clave que descomprime el ZIP
y permite extraer el TXT, cuyo contenido es:

flag{Recursion_1S_T3rribl3_AnD_1_H4t3_My_L1F3!!}
