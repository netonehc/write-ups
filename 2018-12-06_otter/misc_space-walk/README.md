# Space Walk (misc, 200p, 35 solved)

> Enjoy the clip (;

### Detalles relevantes

El input es un video, del cual me llaman la atencion dos cosas:
- En el texto del video hay una parte en la que dice: "THE KEY OF YOUR VICTORY
  IS DETERMINATION"
- Al final hay un QR, que lleva a la URL:
  https://mega.nz/#!EWpmUaYa!CeHsGKCiHKMJdOSXBvn8qcHkcDHp8D9qNNc-PTRLg3g

Parece que la URL lleva al fichero de MEGA es un WAV, un audio de una cancion
(que no conozco).

### Descubriendo el texto ofuscado

Analizando el fichero de audio, resulta que con steghide y la clave
"determination" se obtiene un fichero oculto en la cancion que contiene:

```
$ steghide extract -sf The_Fart.wav -p determination
$ cat steganopayload8977.txt
flag{I_L0vE_0Tt3r_R!ck}
```

Solucion: I_L0vE_0Tt3r_R!ck
