# Binops (misc, 100p, 25 solved)

> Morty found a new "clever" way to encode his data.

### Buscando el patron

Parece que nos dan un fichero en un idioma asiatico. Algunos simbolos parecen
escudos y espadas, pero nada que aporte luz.

```
關雎 父親回後竊聽 在一處 己轉身後竊聽 在一處 己轉身關雎 父親回關雎 父親回後竊聽 在一處 己轉身後竊聽 在一處 己轉身關雎 父親回關雎 父親回後竊聽 在一處 己轉身後竊聽 在一處 己轉身關雎 父親回後竊聽 在一處 己轉身後竊聽 在一處 己轉身關雎 父親回關雎
...
```

Tiro de google translate y parece que lo detecta como idioma Chino, ademas es 
un texto con bloques repetidos segun se observa en la traduccion.

Tras analizarlo detenidamente observo que solo existen dos bloques, que se van 
repidiendo de forma aleatoria. Por lo que parece que sea algun lenguaje 
binario. De entrada solo me viene el morse a la cabeza. Tras probarlo, no se 
adapta al lenguaje y por tanto nisiquiera produce un output.

### Conversion a ASCII

Igual es lenguaje binario simplemente. Pruebo a sustituir los bloques por 0 o 1
desde la web:

http://www.unit-conversion.info/texttools/replace-text/

```
01100110011011000110000101100111011110110011000001110100011101
00011001010111001001110011010111110101001001011111010101000110
100001100101010111110100001000110011011100110111010001111101
```

Tras ello, convierto a ASCII desde la web:

https://www.rapidtables.com/convert/number/binary-to-ascii.html

Bingo: flag{0tters_R_The_B3st}









