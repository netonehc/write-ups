# Secrets (android)

> Can you find my secrets?

Me dan un APK, con `http://www.javadecompilers.com/` extraigo su
source que ni se han molestado en ofuscar, que detalle.

Mirando un poco por encima su estructura, veo que esta traducido a
muchos idiomas, bien.

En la ruta `/resources/res/values/`, la cual se conoce por contener
los strings de la app, en el fichero `strings.xml` encuentro un
atributo llamativo:
```
<string name="flag">Z2lnZW17aW5maW5pdGVfZ2lnZW1zfQ==</string>
```

Decodificado de base64 tengo la flag.

Solucion: `gigem{infinite_gigems}`
