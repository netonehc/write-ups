# -.- (crypto)

> To 1337-H4X0R:
> 
> Our coworker Bob loves a good classical cipher. Unfortunately, he also loves to
> send everything encrypted with these ciphers. Can you go ahead and decrypt this
> for me?

> Difficulty: easy

El input es una cadena de texto que solo contiene las estrcuturas 'dit', 'di' y 'dah'. El enunciado me esta diciendo que tiene que ver con morse. Tal que:
```
dah-dah-dah-dah-dah dah-di-di-dah di-di-di-di-dit dah-dah-di-di-dit dah-dah-di-di-dit dah-dah-dah-dah-dah di-di-dah-dah-dah di-dah dah-di-di-di-dit dah-di-dah-dit di-di-di-di-dit dah-dah-dah-di-dit
...
```

Sustituyo los 'di' y 'dit' por '.'.
Sustituyo los 'dah' por '-'.

El resultado, es una cadena tal que:
```
----- -..- ..... --... --... ----- ..--- .- -.... -.-. ..... ---.. --... ....- ....- --... ..... .---- ...-- ---.. -.... ..... ...-- ---.. --... .---- -.... . -.... -.. ....- -.. ..... ----. ..... ..... ..--- .- --... ...-- --... -.... ....- -.... ....- ---.. -.... -... -.... .- ....- ----. --... ....- ..--- .- ..... ..--- ..... .---- ..--- -.... ....- .- --... ----- ..... .- --... -.... -.... .- -....
etc etc
```

Con el conversor `https://morsecode.scphillips.com/translator.html` obtengo:
```
0X57702A6C58744751386538716E6D4D59552A737646486B6A49742A5251264A705A766A6D2125
254B446B6670235E4E39666B346455346C423372546F5430505A516D4351454B5942345A4D762A
21466B386C25626A716C504D6649476D612525467A4720676967656D7B433169634B5F636C3143
4B2D7930755F683476335F6D3449317D20757634767A4B5A7434796F6D694453684C6D38514546
6E5574774A404E754F59665826387540476E213125547176305663527A56216A21767575703842
6A644E49714535772324255634555A4F595A327A37543235743726784C40574F373431305149
```

Parece hexadecimal, menos por el segundo digito 'X'. Pasando a ASCII toda la
parte valida resulta:
```
Wp*lXtGQ8e8qnmMYU*svFHkjIt*RQ&JpZvjm!%%KDkfp#^N9fk4dU4lB3rToT0PZQmCQEKYB4ZMv*!Fk8l%bjqlPMfIGma%%FzG gigem{C1icK_cl1CK-y0u_h4v3_m4I1} uv4vzKZt4yomiDShLm8QEFnUtwJ@NuOYfX&8u@Gn!1%Tqv0VcRzV!j!vuup8BjdNIqE5w#$%V4UZOYZ2z7T25t7&xL@WO7410QI
```

Y ya se ve el resultado que andaba buscando. Facil.

Solucion: `gigem{C1icK_cl1CK-y0u_h4v3_m4I1}`



	





