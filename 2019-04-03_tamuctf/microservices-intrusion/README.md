# Intrusion (MicroServices)

> Welcome to MicroServices inc, where do all things micro and service
> oriented!
> Recently we got an alert saying there was suspicious traffic on one
> of our web servers. Can you help us out?
> 
> What is the IP Address of the attacker?

Me dan un PCAP.

Simplemente viendo las IPs que interactuan, se observa que entrafico desde
la 10.83.20.77 hacia la 10.91.9.93 es mucho mas elevado que el resto de 
comunicaciones, lo cual podria ser trafico malicioso.

La IP maliciosa resulta ser `10.91.9.93`.


