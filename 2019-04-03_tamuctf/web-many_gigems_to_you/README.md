# Many Gig'ems to you! (web)

> http://web7.tamuctf.com
> 
> Difficulty: easy

Por algun extranya razon, tras 30m dandole vueltas, entiendo que el reto no tiene excesiva logica y empiezo con la purpurina que sale rapido por
suerte.

Un resumen de la situacion:

Primero. Tengo dos rutas
`http://web7.tamuctf.com/index.html`
`http://web7.tamuctf.com/cookies.html`

Segundo. Cada una contiene cientos de imagenes (192 punyos y 531 galletas). Una muestra de `/cookies.html`.
```
<img src="cookie.jpg" alt="cookie">
<img src="cookie.jpg" alt="cookies">
<img src="cookie.jpg" alt="cookie{">
<img src="cookie.jpg" alt="gigs">
<img src="cookie.jpg" alt="gigem">
<img src="cookie.jpg" alt="flag">
```
A priori no tiene mucho sentido, aunque se puede ver texto que podrian 
ser partes de una solucion. WTF!

Luego van las cookies:
```
gigem_continue=cookies}
hax0r=flagflagflagflagflagflag
gigs=all_the_cookies
cookie=flagcookiegigemflagcookie
```

Todas las posibilidades de `/cookies.html` ordenadas:
```
flag
gigs
c00kie
cookie
cookie{
cookies
cookieS
gigem
gigem{
gigem{continued == source_and_
gigem{_continued=source_and_
```

Todas las posibilidades de `/index.html` ordenadas:
```
gigemgigemgigem
gigemmm
gigem
gigem{
gigem{flag_in_
gigsflaggigemflag
gigs
gige
gig{
flaggigs
flagflagflag
```

Con todo esta esta info, llaman mi atencion varias cadenas:
```
gigem_continue=cookies}
gigem{continued == source_and_
gigem{flag_in_
```

Juntandolas resulta ser la flag, no pregunteis porque. Bien es cierto
que incitan a pensar con `continue` que son piezas de puzle, pero la
dificultad en este reto poco tiene que ver con seguridad web.

Solucion: `gigem{flag_in_source_and_cookies}`

