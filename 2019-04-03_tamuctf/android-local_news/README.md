# Local News (android)

> Be sure to check your local news broadcast for the latest updates!
> 
> Difficulty: medium-hard

Tengo otro APK, esta vez con un metodo que realiza operaciones logicas y printea por Log la flag. Como no estoy en modo
debug con cone source sino que dispongo de un JADX, voy a simular la logica de la funcion anyadiendo a mano las
variables de las que tire.

La funcion en cuestion se llama desde `MainActivity`, el cual contiene la siguiente clase:
```
class C00111 extends BroadcastReceiver {
        C00111() {
        }

        public void onReceive(Context context, Intent intent) {
            Log.d(MainActivity.this.getString(C0012R.string.flag), Deobfuscator$app$Debug.getString(0));
        }
    }
```

Si voy a la clase `Deobfuscator$app$Debug` me encuentro:
```
public static String getString(int id) { // id = 0

        int location1Index = id % 4096;
        int location2ChunkIndex = (id + 1) / 4096;
        int location2Index = (id + 1) % 4096;
        String locations1 = locationChunks[id / 4096];
        String locations2 = locationChunks[location2ChunkIndex];
        int offset1 = ((locations1.charAt((location1Index * 2) + 1) & SupportMenu.USER_MASK) << 16) | (locations1.charAt(location1Index * 2) & SupportMenu.USER_MASK);
        int length = ((locations2.charAt((location2Index * 2) + 1) << 16) | locations2.charAt(location2Index * 2)) - offset1;
        char[] stringChars = new char[length];
        for (int i = 0; i < length; i++) {
            int offset = offset1 + i;
            int indexIndex = offset % 8192;
            int index = indexChunks[offset / 8192].charAt(indexIndex) & SupportMenu.USER_MASK;
            int charIndex = index % 8192;
            stringChars[i] = charChunks[index / 8192].charAt(charIndex);
        }
        return new String(stringChars);
    }
```

Por pereza, tiro del compilador online `https://www.jdoodle.com/online-java-compiler`. Simulo la funcion anterior y me queda
tal que:

```
import java.util.Arrays;

public class MyClass {
    
    private static final String[] charChunks = new String[]{"}18m_hanbed3i{0g"};
    private static final String[] indexChunks = new String[]{"\u000f\f\u000f\t\u0003\r\u0005\f\n\n\t\u0007\u0004\u0002\u0001\u0006\t\b\u000e\u0001\u000b\b\t\u0006\u0000"};
    private static final String[] locationChunks = new String[]{"\u0000\u0000\u0019\u0000"};
    public static final int USER_MASK = 65535;
    
    public static void main(String args[]) {
        int id = 0;
        
        int location1Index = id % 4096;
        System.out.println("location1Index: " + location1Index);
        
        int location2ChunkIndex = (id + 1) / 4096;
        System.out.println("location2ChunkIndex: " + location2ChunkIndex);

        int location2Index = (id + 1) % 4096;
        System.out.println("location2Index: " + location2Index);
        
        String locations1 = locationChunks[id / 4096];
        String locations2 = locationChunks[location2ChunkIndex];

        int offset1 = ((locations1.charAt((location1Index * 2) + 1) & USER_MASK) << 16) | (locations1.charAt(location1Index * 2) & USER_MASK);
        System.out.println("offset1: " + offset1);

        int length = ((locations2.charAt((location2Index * 2) + 1) << 16) | locations2.charAt(location2Index * 2)) - offset1;
        System.out.println("length: " + length);
        
        String flag = "";
        
        for (int i = 0; i < length; i++) {
            int offset = offset1 + i;
            int indexIndex = offset % 8192;
            int index = indexChunks[offset / 8192].charAt(indexIndex) & USER_MASK;
            int charIndex = index % 8192;
            flag += charChunks[index / 8192].charAt(charIndex);
        }
        
        System.out.println("flag: " + flag);
        
    }
}
```

Tras ejecutarlo genere al output:
```
location1Index: 0
location2ChunkIndex: 0
location2Index: 1
offset1: 0
length: 25
flag: gigem{hidden_81aeb013bea}
```

Solucion: `gigem{hidden_81aeb013bea}`

