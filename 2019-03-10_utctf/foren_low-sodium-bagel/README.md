# Low Sodium Bagel (foren)

> I brought you a bagel, see if you can find the secret ingredient.
> 
> by balex

```
# file low-sodium-bagel.jpeg 
low-sodium-bagel.jpeg: JPEG image data, JFIF standard 1.01, aspect ratio, density 1x1, segment length 16, baseline, precision 8, 1024x1024, frames 3
```
El input es una imagen, por el enunciado parece que esconde algo.

```
# binwalk low-sodium-bagel.jpeg 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
```
Parece que eso que esconde no esta stackeado bajo la imagen.

```
# exiftool low-sodium-bagel.jpeg 
ExifTool Version Number         : 11.16
File Name                       : low-sodium-bagel.jpeg
Directory                       : .
File Size                       : 309 kB
File Modification Date/Time     : 2019:03:09 19:42:19-05:00
File Access Date/Time           : 2019:03:09 19:42:38-05:00
File Inode Change Date/Time     : 2019:03:09 19:42:19-05:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
JFIF Version                    : 1.01
Resolution Unit                 : None
X Resolution                    : 1
Y Resolution                    : 1
Image Width                     : 1024
Image Height                    : 1024
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:4:4 (1 1)
Image Size                      : 1024x1024
Megapixels                      : 1.0
```
Algunos de sus metadatos, todos parecen en orden.

```
# steghide info low-sodium-bagel.jpeg 
"low-sodium-bagel.jpeg":
  format: jpeg
  capacity: 17.6 KB
Try to get information about embedded data ? (y/n) y
Enter passphrase: 
  embedded file "steganopayload4837.txt":
    size: 37.0 Byte
    encrypted: rijndael-128, cbc
    compressed: yes
```
Bingo, un fichero cifrado con `steghide`.

```
# steghide extract -sf low-sodium-bagel.jpeg 
Enter passphrase: 
wrote extracted data to "steganopayload4837.txt".

# cat steganopayload4837.txt 
utflag{b1u3b3rry_b4g3ls_4r3_th3_b3st}
```

Solucion: `utflag{b1u3b3rry_b4g3ls_4r3_th3_b3st}`



