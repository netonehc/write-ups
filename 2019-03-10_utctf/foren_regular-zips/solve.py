import os
import zipfile
import exrex

def main():

    # variables
	zip_filename = "archive.zip"
	regex_filename = "hint.txt"
	count = 1
	root_path = "/root/Downloads/utctf/foren_regular-zips_no/lab/"

	while True:

		# set CURRENT and NEXT paths
		if count == 1:
			path = root_path+str(count)+"/"
		else:
			path = next_path

		#count = count+1
		count = 1001
		next_path = root_path+str(count)+"/"

		# load zip and regex input files
		zip = zipfile.ZipFile(path+zip_filename)
		regex = open(path+regex_filename, "r").read()
		dic = list(exrex.generate(regex))

		# create output folder
		os.system("mkdir "+next_path)

		print("Path:      %s" % path)
		print("Next path: %s" % next_path)
		print("Regex:     %s" % regex)

		# find the password
		for pwd in dic:
			try:
				if count == 1001:
					zip.extract(path=next_path,member='flag.txt',pwd=pwd)
				else:
					zip.extract(path=next_path,member=zip_filename,pwd=pwd)
					os.system("unzip -q -d "+next_path+" -P '"+pwd+"' "+path+zip_filename+" "+regex_filename)
					print('Password:  %s' % pwd)
					break
			except:
				pass
		print('\n')
if __name__ == '__main__':
	main()
