# Regular Zips (foren)

> ^ 7 y RU[A-Z]KKx2 R4\d[a-z]B N$
> 
> https://storage.googleapis.com/utctf/RegularZips.zip

En enunciado contiene una serie de caracteres/filtros que no
comprendo de primeras.

```
# file RegularZips.zip 
RegularZips.zip: Zip archive data, at least v1.0 to extract
```
En input es un ZIP, ademas con credenciales, que contiene 2 ficheros:
- `hind.txt` que intuyo sera una pista para acabar el chall
- Un fichero `archive.zip` que entiendo que contiene la flag

Vale, los codigos del enunciado deben ser una regex de la pass.

Resulta ser una bomba ZIP de 1000 iteraciones en la que en cada una de
ellas la password cumple un regex diferente.

Con la ayuda de tool `exrex` genero diccionarios de posibles passwords que
cumplen una regex. Con el script `solve.py` consigo sacar la flag. Tiempo
de procesado:
```
real	95m16.786s
user	52m28.639s
sys	2m6.880s
```

Solucion: `utflag{bean_pure_omission_production_rally}`



