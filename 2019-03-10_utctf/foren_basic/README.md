# Basics (foren)

> My friend said they hid a flag in this picture,
> but it's broken!
> 
> by balex

```
# file secret.jpg 
secret.jpg: ASCII text
```
En input parece una imagen, por el enunciado, pero esta corrupta.

```
# xxd secret.jpg | head -n 5
00000000: 7574 666c 6167 7b64 306e 745f 7472 7535  utflag{d0nt_tru5
00000010: 745f 6631 6c33 5f33 7874 336e 7331 306e  t_f1l3_3xt3ns10n
00000020: 357d 0a                                  5}.
```
Sin hacer nada, la flag...vaya, en titulo no miente.

Solucion: `utflag{d0nt_tru5t_f1l3_3xt3ns10n5}`



