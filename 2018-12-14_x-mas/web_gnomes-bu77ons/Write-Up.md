## Datos Iniciales

Se obtiene una dirección IP en la que está hospedado el reto. Esta dirección apunta a un servidor web.

## Resolución

#### Se accede a la dirección IP obtenida de los creadores del reto.

![5c1b98cc28ffd](https://i.loli.net/2018/12/20/5c1b98cc28ffd.png)

#### Se analiza la página web.

![5c1b99b07f338](https://i.loli.net/2018/12/20/5c1b99b07f338.png)

#### Modificamos el Código fuente

En el código observamos un botón con el valor "Click here boai".

Modificamos el código de la página para que el valor mandado en el POST sea "flag".

![5c1b99bc26dfa](https://i.loli.net/2018/12/20/5c1b99bc26dfa.png)

![5c1b98b821c3f](https://i.loli.net/2018/12/20/5c1b98b821c3f.png)

#### Resultado

El servidor web nos devuelve la página con la flag: X-MAS{PhPs_A1n7_m4d3_f0r_bu77_0ns___:)}

![5c1b9899d006a](https://i.loli.net/2018/12/20/5c1b9899d006a.png)
