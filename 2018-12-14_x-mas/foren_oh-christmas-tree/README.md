# Oh Christmas Tree (foren, 25p, 750 solved)

> Santa is sure that he left a gift somewhere around here. Can you help him 
> find it?

Tenemos una imagen.

```
# strings Merry\ Christmas.jpg > strings.txt
# grep "X-MAS" strings.txt 
Copyright (c) 1998 Hewlett-X-MAS{0_Chr15tm
```

Volcamos los strings del fichero y filtramos por el formato de la flag. 
Parece que hay matches...  Mirando el fichero strings.txt veo:

```
...
text
Copyright (c) 1998 Hewlett-X-MAS{0_Chr15tm
desc
sRGB IEC61966-2.1
sRGB IEC61966-2.1
XYZ 
XYZ 
XYZ 
XYZ 
XYZ 
desc
IEC as_tr33_1s_th1s_a
IEC _flag_i_wond3r}..
desc
...
```

Juntando las 3 partes sale la flag.

Solucion: X-MAS{0_Chr15tmas_tr33_1s_th1s_a_flag_i_wond3r}
