# Santa's lucky number (web, 50p, 152 solved)

> Come on! Santa's lucky number is pretty predictable, don't you think? ;)
> 
> Server: http://199.247.6.180:12005

### Bucle de peticiones GET

Parece ser que hay una clave en alguna pagina de todas las que se listan.

Genero un script en bash para almacenar las claves en un fichero.

```bash
for ((i=1;i<=10000;i++)); 
do 
    html=$(curl -s http://199.247.6.180:12005/?page=$i | tail -1 |  grep -o -P '.*(?=</p>)')
    echo -e "${html}" >> flags.txt
done
```

La muestra del fichero contiene registros tal que:

```
...
0dde155cc0ad6c364bbea9f368ea3ee793d8507fe96f8475a6d4c332b4909296957d89c0a7400f5db35a1d43e2ead5b5d0f26832e85aa04102031820bbe8a68d
7bf8b32bfe78063da8a6caff5ffc08205d2a809e128f796b3b1192a70e3d9bf77c3a6e6a47314f7a1b8954b15f4e1499924dda2687f65156b7214469e538b9c7
X-MAS{W00pS_S0m30n3_73l1_S4n7a_h1s_c00k1eS_Ar3_BuRn1ng}
1a99a39627392c4297810bf142685a2882c179ad1e7271533bd076ade957e531f4a2d5a85530e4d62200bf174ca4cdb617b7ca7e9457d22e1c1447fa872bfeb0
e6a3a83cede45e325b4503eccd789914bf998f3bae49bbf4a98a63217cc8572e54737457510c33f5841e9516c260652447c29312846b7acda62efa65d230ccdc
...
```

Tras obtener mas de 1500 claves me encuentro:

```
# cat flags.txt | grep "X-MAS"
X-MAS{W00pS_S0m30n3_73l1_S4n7a_h1s_c00k1eS_Ar3_BuRn1ng}
```

Solucion: X-MAS{W00pS_S0m30n3_73l1_S4n7a_h1s_c00k1eS_Ar3_BuRn1ng}
