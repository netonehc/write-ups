# Pinkie's gift (50p)
> _In this challenge, Santa Pie will give you some unusual gifts. See if you can use this wisdom to get the flag before Christmas._

En este desafio se nos proporciona un binario, una dirección ip y un puerto.

Nuestro objetivo, como en la mayoria de los desafios `pwn` es analizar el binario en busca de vulnerabilidades, desarrollar un exploit, y ganar RCE (**R**emote **C**ode **E**xecution)


## Análisis del binario

Cuando ejecutamos por primera vez el binario podemos ver que nos dan 2 direcciones de memoria, que resultan ser (de izquierda a derecha) una direccion de memoria estática que podremos utilizar en el caso de que queramos guardar algo en memoria para recuperarlo más tarde, y la direccion de memoria donde se encuentra `system`.

![](https://gitlab.com/_marc.b/write-ups/raw/master/X-MAS%202018/pinkie-gift/screenshots/chall_at_start.png)

Ahora, el programa toma 2 inputs. Uno nos lo devolverá en pantalla y otro simplemente lo guardará en memoria.

![](https://gitlab.com/_marc.b/write-ups/raw/master/X-MAS%202018/pinkie-gift/screenshots/full_chall.png)

Si abrimos el binario con gdb, este sería el apartado que nos interesa:

![](https://gitlab.com/_marc.b/write-ups/raw/master/X-MAS%202018/pinkie-gift/screenshots/asm_analized.png)

Además, he marcado en la imagen las partes vulnerables del programa, hablaré de ellas en el siguiente apartado.



## Vulnerabilidades

### FSE (Format String Exploit)

En `main <+162>` podemos encontrar la vulnerabilidad que podría ser utilizada para crear un FSE.

Aquí podemos ver la prueba de la existencia de la vulnerabilidad:

![](https://gitlab.com/_marc.b/write-ups/raw/master/X-MAS%202018/pinkie-gift/screenshots/FSE.png)

### BOF (Buffer Overflow)

En `main <+177>` podemos encontrar un `call` a `gets@plt` función conocida por ser completamente insegura.

`_IO_gets` recibe un input del usuario y lo almacena en una dirección de memoria, sin embargo, no comprueba la cantidad de bytes escritos a esa región.
Esto nos permite realizar un BOF, pudiendo saltar a cualquier dirección de memoria.

Aquí vemos como el programa crashea:
![](https://gitlab.com/_marc.b/write-ups/raw/master/X-MAS%202018/pinkie-gift/screenshots/BOF.png)

Y aquí vemos que intenta saltar a `0x41414141` (EIP = 0x41414141):

![](https://gitlab.com/_marc.b/write-ups/raw/master/X-MAS%202018/pinkie-gift/screenshots/BOF_EIP.png)



## El exploit

La estructura del exploit sera la siguiente:
1. Guardar la dirección de memoria estática que da el programa en su inicio (0x8049940)
1. Guardar la dirección de memoria de `system` que da el programa en su inicio (0xf7dca200). Puede variar.
1. No nos aprovecharemos de la vulnerabilidad FSE, no nos hace falta. Por lo tanto enviaremos un input cualquiera para pasar al `_IO_gets`
1. Enviamos el payload1
1. Enviamos el payload2
1. Interactuamos con la shell

La estructura del payload1 es la siguiente:
> [PADDING] + [DIRECCION_DEL_2do_ROP] + [main <+177> (AQUí esta el `call` de `_IO_GETS`)] + [DIRECCION_BINSH]

La estructura del payload2 es la siguiente:
> "AAAAA" + "sh\0" + "AAAA" + [DIRECCION_SYSTEM] + "JUNK" + [DIRECCION_BINSH]

## Tips

Es complejo explicar el proceso de crear el exploit detalladamente, y complejo de entender si se tiene poca o ninguna experiencia en la categoría `pwn`.
Por eso, para comprender totalmente la solución, aconsejaría acompañar esta explicación de una terminal con `gdb` viendo como se comporta el programa
cuando recibe los inputs del exploit, y también probar a modificar el exploit para entender su funcionamiento.

Podeis descargar el exploit (xpl.py) y el binario (pinkiegift) desde este repositorio. 

Para debuggear el programa mientras recibe el ataque, yo hago lo siguiente:
- Ejecutar el exploit y esperar a que llegue a la primera pausa

![](https://gitlab.com/_marc.b/write-ups/raw/master/X-MAS%202018/pinkie-gift/screenshots/pause.png)
- En otra terminal ejecutar el siguiente comando: `$ gdb -p $(pidof pinkiegift)` y examinar el programa conforme va recibiendo los inputs del exploit.

Y por último, me gustaría recomendar el siguiente plugin para `gdb`: <https://github.com/pwndbg/pwndbg>

