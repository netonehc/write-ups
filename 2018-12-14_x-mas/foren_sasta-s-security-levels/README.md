# Santa's Security Levels (foren, 50p, 378 solved)

> Santa has a flag hidden for you! Find out where. All letters, except for the 
> X-MAS header, are lowercase.
> (Flag is Non-Standard, please add X-MAS{ } to the found string)
> message.mp3

Bueno, nos dan un fichero de audio. Abriendolo con Sonic Visualizer, el
espectograma nos muestra una secuencia de tonos que parecen morse:

```
--. .. - .... ..- -... -.-. --- -- --. --- --- --- --. .- .-.. -..- -- .- ...
```

Que traducido es "GITHUBCOMGOOOGALXMAS":

```
--. .. - .... ..- -... GITHUB
-.-. --- -- COM
--. --- --- --- --. .- .-.. GOOOGAL
-..- -- .- ... XMAS
```

Buscando el github el usuario llamado "GOOOGAL", que tiene un repositorio 
llamado: https://github.com/Gooogal/xmas ...

Su contenido es un fichero que nos dice que descifremos el texto:

```
vF ur uNq nAlguvat pbasvqraGvNy gb fnl, ur jebgr Vg ia pvcure, gung vF, ol FB punaTvat gur beqre bs gur Yrggref bs gur nycuNorg, gung abg n jbeQ pbhyq or ZnQR bHg.
```

Parece un ROT 13..decodificandolo en https://www.asciitohex.com/ se obtiene:

```
iS he hAd aNything confidenTiAl to say, he wrote It vn cipher, that iS, by SO chanGing the order of the Letters of the alphAbet, that not a worD could be MaDE oUt.
```

Cogiendo solo las mayusculas se obtiene el texto "SANTAISSOGLADMDEU".

Solucion: X-MAS{santaissogladmdeu}

