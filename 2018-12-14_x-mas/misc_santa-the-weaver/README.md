# Santa The Weaver (misc, 25p, 809 solved)

> Santa LOVES two things! Weaving and ...

Tenemos un reto sencillo. El dato es un zip que dentro contiene un png.

```
$ strings flag.png
Uy2y
03Y^\
B 7&
Spvl*B
,/vW
IEND
X-MAS{S4n7a_l1k3s_h1di()g_gif7$}
```

Solucion: X-MAS{S4n7a_l1k3s_h1di()g_gif7$}
