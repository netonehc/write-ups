# BoJack Horseman's Sad Christmas (misc-foren, 128p, 92 solved)

> BoJack recieved a Christmas card from Diane but Princess Carolyn shredded it 
> to bits. It looks like festive barf now! Can you help BoJack read the card?

### Bit a bit se hace camino

Nos dan es una imagen .PNG, donde se aprecia claramente los bits que la
componen. Por la estrcutura para ser algun tipo de informacion ofuscada a 
descifrar.

Con la ayuda de la tool online https://littlevgl.com/image-to-c-array
consigo obtener los valores en hexadecimal de cada posicion, tal que:

```
#include "lv_conf.h"
#include "lvgl/lv_draw/lv_draw_img.h"

const uint8_t original-hex-dump_map[] = {
#if LV_COLOR_DEPTH == 1 || LV_COLOR_DEPTH == 8
  /*Pixel format: Red: 3 bit, Green: 3 bit, Blue: 2 bit*/
  0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0x10, 0xc0, 0xc0, 0x10, 0x10, 0x10, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0xc0, 0x10, 0x10, 0x10, 0x10, 0x10, 0xc0, 0x10, 0x10, 0xc0, 0x10, 0xc0, 0x10, 0x10, 0xc0, 0x10, 0x10, 0x10, 0xc0, 0xc0, 0x10,
  ...
```

Como se aprecia, los valores posibles que toma la imagen son "0xc0" o "0x10".

Sustituyendo estos dos valores por 1 y 0 con la ayuda de alguna tool online 
como http://www.unit-conversion.info/texttools/replace-text/

### De binario a renderizado

Se obtiene todo el fichero en binario, se transforma a base64 con la tool
online https://www.asciitohex.com/

Y el resultado se renderiza con alguna tool online como
https://codebeautify.org/base64-to-image-converter donde se puede apreciar la
solucion.

Solucion: X-MAS{1_L0V3_B0J4ckH0rs3m4n}
