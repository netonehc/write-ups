# Random Present (50p)
> _Sometimes you just don't get what you desire..._

En este desafio se nos proporciona un binario y la dirección donde esta siendo ejecutado el servicio.

## Análisis del binario

Lo primero que vemos al desensamblar el binario es un `call` a `gets@plt`, y esto nos informa
evidentemente de que hay un _buffer overflow_

![](https://gitlab.com/_marc.b/write-ups/raw/master/X-MAS%202018/random-present/screenshots/gets.png)

Pero llegados a este punto algo me llamó la atención. Es evidente que la forma de explotar esta
vulnerabilidad es haciendo un ROP, pero, ¿Por qué no nos dan el binario de libc?

En ese momento deduje que era solo para dar un punto más de dificultad al desafio, y la idea
que pensé para averiguar que versión de libc estaba utilizando fue fijarme en los últimos 2 bytes
de una dirección de libc (por ejemplo `_IO_puts`) y utilizar ![esta](https://libc.blukat.me/) página para averiguar que versión era exáctamente.

Lo siguiente que debímos hacer era conseguir un `leak` para poder ver la dirección de `_IO_puts`
y utilizar los últimos 2 bytes para determinar la versión de libc.

Para ello, yo utilicé `ropper`, un programa que busca pequeñas porciones de código ensamblador (gadgets)
en un binario y nos muestra en que dirección de memoria se encuentra. https://github.com/sashs/Ropper

Con esta aplicación, busque un `pop rdi; ret` para poder controlar el registro RDI y escribir ahí la
dirección de memoria de `puts@got`, después salté a un `call puts@plt` y así hice que `_IO_puts`
imprimiera en pantalla el contenido de `puts@got` es decir `_IO_puts`.

Pero como podemos ver aquí, los últimos 2 bytes no coinciden para nada, y deberían ser identicos...

![](https://gitlab.com/_marc.b/write-ups/raw/master/X-MAS%202018/random-present/screenshots/remote_libc_discordance.png)

Esto solo tiene una explicación, y es que el servicio que está ejecutando en el servidor, cada vez
que arranca, cambia de versión de libc y por lo tanto los `offsets` cambian.

## El exploit

Para hacer frente a esta adversidad podríamos intentar crear una base de datos con los offsets de puts
y dependiendo del offset que devolviera el servicio utilizar un libc u otro para calcular la posicion
de `system`, pero esto habría sido muy costoso, así que optamos por la versión española, que es: ir
probando con una versión de libc hasta que coincida y consigamos RCE.

Podéis ver más detalles acerca del exploit leyendo el código fuente que se encuentra en este
mismo repositorio.



