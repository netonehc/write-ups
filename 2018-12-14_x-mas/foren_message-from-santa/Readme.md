### Message from Santa (foren, 50p, 306 Solves)

> Santa prepared a flag for you. Unfortunately, he has no idea where he left it. Finders keepers.

Se proporciona un archivo llamado `classified_gift_distribution_schema.img`.

1. Analizar el archivo proporcionado.

- Por la extensión `.img` se puede deducir que es una imagen de archivo de disco.

```BASH
$ file classified_gift_distribution_scheme.img
```

- Es una imagen de disco de USB particionada con DOS/MBR con formato FAT (vfat).

1. Montar la imagen de USB

```BASH
$ sudo mount classified_gift_distribution_scheme.img -t vfat -o loop,ro,noexec /mnt
```

1. Es preciso situarse en la carpeta `/mnt` para ver qué archivos contiene la imagen una vez montada.

```BASH
$ cd /mnt/
```

1. Nos encontramos con un archivo `grep.jpeg`. Sin embargo, si listamos todos los archivos con `ls -a` (**INCLUIDOS LOS OCULTOS**), se descubre una carpeta nueva llamada `.Trash-0`.

> Los dispositivos USB tienen una carpeta `.Trash-x` donde se almacenan archivos eliminados. Esta carpeta contiene dos subcarpetas *files* y *info*.
> **files**: contiene los archivos
> **info**: contiene la ruta que tenían los archivos en el USB antes de ser borrados y su fecha de eliminación.

1. Entrar en la carpeta **files**.

```BASH
$ cd ./.Trash-0
$ cd files
```

Dentro de la carpeta hay varias imágenes que son partes de una imagen completa. Se deben ordenar como un puzzle y de ahí se saca la flag.
