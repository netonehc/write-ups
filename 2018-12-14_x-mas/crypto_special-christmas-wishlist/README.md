# Special Christmas Wishlist (crypto, 50p, 124 solved)

> While Santa was looking through the wishlists of the childern all around the world he came across a very strange looking one. Help Santa decode the letter in order to fulfill the wishes of this child.
> (Flag is Non-Standard)
> wishlist.png
> UPDATE: flag is lowercase!

### Analizando el input

Son runas, pero no del mismo lenguaje. Algunos ejemplos de estos grupos de
simbolos: https://es.wikipedia.org/wiki/Alfabeto_r%C3%BAnico

Literal, estube 5h intentando matchear los simbolos con caracteres ascii para
conseguir un texto en ingles leible y con sentido en el contexto del problema.

### Cifrado bidireccional por sustitucion

No tuve exito, por lo que tire de substitution ciphers. Me defini un caracter
para cada simbolo y los meti en este https://www.guballa.de/substitution-solver
que me daba opcion de diccionarios por idioma, por lo que seleccinando English y
escribiendo el siguiente texto:

```
ABCDCEFG AHIJDCEFG KHELG LDJI
JDMBNNG NBODAP QHHRGIFL
KCOA QGGM JABLLGL LGC HN CSH
QBF FHJ SDLFHO CEOQAGML
BFTGICEMGM OEACDCHHA UADV SBCUK
CSDJ OBMLKOBAAHS LRGSGM
IGLCDIJ LCHMBJG UHICBDIGML
LODADIJ WDXH JBMFGI LUEAVCEMG
AHIJ FDLCBIUG CHEUK ABOV
OEACDUHAHM HOQMG LCGOAGLL SDIG JABLL LGC
```

Me muestra el restultado que tiene "sentido":

```
LATITUDE LONGITUDE HOUSE SIGN
GIRAFFE FAMILY BOOKENDS
HTML BEER GLASSES SET OF TWO
BAD DOG WISDOM TUMBLERS
ADVENTURER MULTITOOL CLIP WATCH
TWIG MARSHMALLOW SKEWER
NESTING STORAGE CONTAINERS
SMILING JIZO GARDEN SCULPTURE
LONG DISTANCE TOUCH LAMP
MULTICOLOR OMBRE STEMLESS WINE GLASS SET
```

El mapa de cifrado que me da es: bqufgnjkdwraoihvzmlcetsypx

Metiendo la flag cifrada, que parece que es la ultima linea de simbolos y el
mapa de sustitucion en la web: https://www.dcode.fr/monoalphabetic-substitution
resulta en lo siguiente:

```
INPUT:      ZOBLPHEBMGLHJHHFBCLEQLCDCECDHIUDVKGML
MAP CIPHER: bqufgnjkdwraoihvzmlcetsypx
OUTPUT:     QMASYOUARESOGOODATSUBSTITUTIONCIPHERS
```

En este caso, como solo he traducido 6 lineas de un texto de al menos 30, se ha
producido aparentemente un solo error en el primer caracter de la frase, pero
seems legit. Efectivamente es la flag. En minusculas como indica en enunciado.

Solucion: xmasyouaresogoodatsubstitutionciphers
