import hashlib

filename = 'input.txt'

with open(filename, 'r') as file :
  filedata = file.read()

alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-=[]{}\\|;:"\',<.>/?'

for c in alphabet:

    md5 = hashlib.md5(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(md5, c)

    sha1 = hashlib.sha1(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha1, c)

    sha224 = hashlib.sha224(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha224, c)

    sha256 = hashlib.sha256(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha256, c)

    sha384 = hashlib.sha384(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha384, c)

    sha512 = hashlib.sha512(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha512, c)

with open(filename, 'w') as file:
   file.write(filedata)
