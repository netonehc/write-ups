# Alphabet (crypto, 60p, 159 solved)

> If you know your keyboard, you know the flag
>
> Input: submit_the_flag_that_is_here.txt

### Cifrado MD5 y SHA

El fichero adjunto contiene mas de 4MB de texto sin saltos de linea, que consiguio colgarme la VM durante 2m.

Muestra de las primeras lineas:

```
72dfcfb0c470ac255cde83fb8fe38de8a128188e03ea5ba5b2a93adbea1062fa 65c74c15a686187bb6bbf9958f494fc6b80068034a659a9ad44991b08c58f2d2 454349e422f05297191ead13e21d3db520e5abef52055e4964b82fb213f593a1 3f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea 6f8f57715090da2632453988d9a1501b b14a7b8059d9c055954c92674ce60032 865c0c0b4ab0e063e5caa3387c1a8741
...
```

Buscando cualquiera de los strings del fichero en google se observa que son caracteres ASCII cifrados con MD5 y SHA.

Sin pensarmelo, genero un script que sustituya un hash por si correspondiente char. Me queda el python `solve.py` tal que:

```
import hashlib

filename = 'input.txt'

with open(filename, 'r') as file :
  filedata = file.read()

alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-=[]{}\\|;:"\',<.>/?'

for c in alphabet:

    md5 = hashlib.md5(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(md5, c)

    sha1 = hashlib.sha1(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha1, c)

    sha224 = hashlib.sha224(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha224, c)

    sha256 = hashlib.sha256(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha256, c)

    sha384 = hashlib.sha384(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha384, c)

    sha512 = hashlib.sha512(c.encode('utf-8')).hexdigest()
    filedata = filedata.replace(sha512, c)

with open(filename, 'w') as file:
   file.write(filedata)
```

Haciendo una busqueda rapida en el fichero del string `F # {` se encuentra la cadena:
```
Congratulations!_T#e_Flag_Is_F#{Y3aH_Y0u_kN0w_mD5_4Nd_Sh4256}_Done
```

Solucion: `F#{Y3aH_Y0u_kN0w_mD5_4Nd_Sh4256}`
