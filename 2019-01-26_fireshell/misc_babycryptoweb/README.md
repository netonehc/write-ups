# babycryptoweb (misc, 60p, 168 solved)

> Can you help me recover the flag?
>
> Input: https://babycryptoweb.challs.fireshellsecurity.team/

## Servidor

En el input se da el codigo que corre el servidor con el que hay que interactuar:
```
<?php

$code = '$kkk=5;$s="e1iwZaNolJeuqWiUp6pmo2iZlKKulJqjmKeupalmnmWjVrI=";$s=base64_decode($s);$res="";for($i=0,$j=strlen($s);$i<$j;$i++){$ch=substr($s,$i,1);$kch=substr($kkk,($i%strlen($kkk))-1,1);$ch=chr(ord($ch)+ord($kch));$res.=$ch;};echo $res;';

if (isset($_GET['p']) && isset($_GET['b']) && strlen($_GET['b']) === 1 && is_numeric($_GET['p']) && (int) $_GET['p'] < strlen($code)) {
    $p = (int) $_GET['p'];
    $code[$p] = $_GET['b'];
    eval($code);
} else {
    show_source(__FILE__);
}

?>   
```

Se aprecia una cadena de codigo que se manipula posteriormente y una serie de
parametros pasados por la query. La flag se mostrara con `eval($code);`.

## Corrigiendo el fallo

Hay que transformar el contenido de `$code`, concretamente un solo `char` en
una de las 235 posiciones del string.

El script realiza una pasada por todos los chars de `$code` y les suma 55 a su
valor ASCII. Es realmente simple. Este proceso resulta en unos valores ASCII
fuera del rango ~65/120 donde se situa el abecedario, y por tanto se muestra el
famoso diamante negro (error de renderizado).

> Tabla ASCII: http://www.asciitable.com/

Pintando de forma [bonita](http://phpbeautifier.com/beautify.php) el contenido de `$code` tenemos:
```
$kkk = 5;
$s = "e1iwZaNolJeuqWiUp6pmo2iZlKKulJqjmKeupalmnmWjVrI=";
$s = base64_decode($s);
$res = "";

for ($i = 0, $j = strlen($s); $i < $j; $i++)
	{
	$ch = substr($s, $i, 1);
	$kch = substr($kkk, ($i % strlen($kkk)) - 1, 1);
	$ch = chr(ord($ch) + ord($kch));
	$res.= $ch;
	};
echo $res;
```

Tras analizarlo, llama especialmente la atencion la linea:
```
$ch = chr(ord($ch) + ord($kch));
```

Justamente es la responsable de que los valores ASCII decodificados del base64 se hagan
mas grandes. Si se sustituye el char `+` por `-` se consigue obtener chars renderizables
que resultan ser la flag.

El script que hice para comprobarlo es:
```
<?

header('Content-type: text/plain');

$kkk=5;
$s=base64_decode("e1iwZaNolJeuqWiUp6pmo2iZlKKulJqjmKeupalmnmWjVrI="); //longitud: 35
echo "\nCODE START: $s\n\n";
$res="";

for($i=0, $j=35; $i<$j; $i++){

  $ch=substr($s,$i,1); //recorre $s cogiendo el siguiente char en cada iteracion
  $intch = ord($ch);
  echo "ch:$intch substr:";

  $kch=substr($kkk,($i%strlen($kkk))-1,1);
  echo " kch:$kch ";

  $ch=chr(ord($ch)+ord($kch)); // FLAG: aqui esta el fallo, el signo debe ser negativo
  $intch = ord($ch);
  echo "ch:$intch \n";

  $res.=$ch;


};
echo "\nCODE END: $res \n\n";

?>
```

Solucion: `F#{0n3_byt3_ru1n3d_my_encrypt1i0n!}`
