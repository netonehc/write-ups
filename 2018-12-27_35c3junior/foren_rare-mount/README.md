# rare_mount (foren, 44p, 209 solved)

> Little or big, we do not care!
>
> FS
>
> Difficulty estimate: Easy

Nos dan un binario.

```
# file ffbde7acedff79aa36f0f5518aad92d3-rare-fs.bin
ffbde7acedff79aa36f0f5518aad92d3-rare-fs.bin: data

# binwalk ffbde7acedff79aa36f0f5518aad92d3-rare-fs.bin

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JFFS2 filesystem, big endian
```

Parece que se trata de un sistema de archivos JFFS2 (Jefferson).

Buscando un poco veo un decompilador "https://github.com/sviehb/jefferson" que instalo:
```
# git clone https://github.com/sviehb/jefferson.git jefferson
# cd jefferson
# python setup.py install
# pip install cstruct
# apt-get install python-lzma
```

Lanzo el decompiler:
```
# jefferson ../ffbde7acedff79aa36f0f5518aad92d3-rare-fs.bin -d ../jefferson_decompile
dumping fs #1 to /root/Downloads/35c3_junior/foren-rare_mount-no/jefferson/../jefferson_decompile/fs_1
Jffs2_raw_dirent count: 2
Jffs2_raw_inode count: 2155
Jffs2_raw_summary count: 0
Jffs2_raw_xattr count: 0
Jffs2_raw_xref count: 0
Endianness: Big
writing S_ISREG RickRoll_D-oHg5SJYRHA0.mkv
writing S_ISREG flag
```

Ya se ve que ha generado un fichero llamado "flag", que en su interior contiene:

`35C3_big_or_little_1_dont_give_a_shizzle`
