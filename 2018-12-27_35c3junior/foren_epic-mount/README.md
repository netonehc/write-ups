# epic_mount (foren, 111p, 45 solved)

> A little bit of stego. Not every header field looks like the other.
>
> FS
>
> Difficulty estimate: Easy-Medium

Tengo otro binario, como en el chal `rare-mount`. Tambien es un sistema de ficheros JFFS2.

```
# binwalk  f9be7cb88a778615216f212d58f62ea3-epic-fs.bin

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JFFS2 filesystem, big endian
```

Al decompilarlo, dan errores los numeros magicos, vamos a arreglarlos.

Se comprueba que la diferencia entre ambos ficheros son solo 31 posiciones. 30 de ellas son bytes aislados y la ultima es el fichero "flag" del reto anterior que no procede.

Sacando con wxHexEditor las diferencias, se obtiene la flag.

Solucion: `35C3_hide_me_baby_one_more_time`

El reto a acabado, lo que hay a continuacion es extra.

Durante la resolucion de reto, profundice en el formato del filesystem, y dejo
por aqui algunas ideas que conclui.


Documentacion usada:
- https://en.wikipedia.org/wiki/File_system (preliminares)
- https://en.wikipedia.org/wiki/JFFS2 (introduccion suave)
- https://www.sourceware.org/jffs2/ (fuente principal)
- https://www.sourceware.org/jffs2/jffs2-slides-transformed.pdf (presentacion)
- http://linux-mtd.infradead.org/~dwmw2/jffs2.pdf (specs en PDF)
- https://www.sourceware.org/jffs2/jffs2-html/ (specs en HTML)
- http://www.inf.u-szeged.hu/projectdirs/jffs2/jffs2-anal/node4.html (formatos de trama)
- http://wiki.emacinc.com/wiki/Mounting_JFFS2_Images_on_a_Linux_PC (cacharreo)

Filesystem: reglas logicas usadas para gestionar grupos de informacion y sus nombres.

JFFS:
- Creado por Axis Communications AB en 1999
- Registros estructurados
  - Los datos se almacenan en cualquier posicion
  - Los packetes (llamados nodos) de datos se escriben de forma
    secuencial, conteniendo una serie de datos:
    - Id del fichero al que pertenece
    - Campo de version, que indica la secuencia cronologica de los
      nodos a los que pertenece este fichero
    - Metadata del inode: uid, gid y mas
    - Mas datos opcionales
  - Solo tiene un tipo de nodo

JFFS2(Journalling Flash File System version 2)
- Mantiene la mayoria de features de la version 1
- Sistema de ficheros especializado en memorias Flash
- Companible con sistemas operativos basados en GNU/Linux y eCos
  creado por RedHat para sistemas embebidos
- Funciona sobre dispositivos NAND (puerta NOT AND)
  - Emplea bloques de 512 bytes
  - Cada bloque reserva 16 bytes para metadata/ECC
- Algoritmos de compresion soportados: zlib, rubin, rtime
- Permite enlaces duros
- Tiene diferentes tipos de nodos, a diferencia de JFFS
- Tiene ideas originarias del filesystem ext2.

Estados de los bloques de memoria:
- clean_list: contiene solo nodos validos
- diry_list: contiene uno o mas nodos obsoletos
- free_list: no contiene nodos y esta preparado para ser rellenado
- Hay mas tipos...

Plantilla mas comun de campos que comparten los nodos:
- 2 bytes: magic bitmask (con valor fijo 0x1985)
- 2 bytes: node type
- 4 bytes: total node length
- 4 bytes: node header CRC (Cyclic redundancy checksum)

Valores del campo "Node type", son 4:
- JFFS2_FEATURE_INCOMPAT
- JFFS2_FEATURE_ROCOMPAT (read only)
- JFFS2_FEATURE_RWCOMPAT_DELETE (read and write): el garbage
  collector lo borra
- JFFS2_FEATURE_RWCOMPAT_COPY (read and write): el garbage
  collector lo copia a una posicion nueva

Tipos de nodos, son 3:
- JFFS2_NODETYPE_DIRENT (dirent) (valor: 0xE001)
  - Suelen tener un tamanyo pequenyo
  - Se define como "Directory entry" o "enlace a un inode"
  - Parametros que contiene:
    - Numero de su inodo padre (dirent)
    - Version: id. de secuencia que ha usado el padre
    - Nombre del enlace al que apunta
    - Numero de inodo al que apunta

  - Ejemplo 1

19 85 E0 01 00 00 00 42 0A 2C FD DB 00 00 00 01 00 00 00 00 00 00 00 02 5C 1B 5E 53 1A 08 00 00 9E E4 A8 03 58 C8 F1 64 52 69 63 6B 52 6F 6C 6C 5F 44 2D 6F 48 67 35 53 4A 59 52 48 41 30 2E 6D 6B 76 FF FF

    - 2 [19 85]: magic numbers del nodo, valor fijo
    - 2 [E0 01]: tipo dirent, valor fijo
    - 4 [00 00 00 42]: tamanyo del inodo, 66 bytes
    - 4 [0A 2C FD DB]: header CRC checksum del nodo
    - 4 [00 00 00 01]: pino, 1
    - 4 [00 00 00 00]: version, 0
    - 4 [00 00 00 02]: ino (=0 para desenlazar), 2
    - 4 [5C 1B 5E 53]: mctime
    - 1 [1A]: nsize
    - 1 [08]: type
    - 1 [00 00]: unused[2]
    - 4 [9E E4 A8 03]: node_crc
    - 4 [58 C8 F1 64]: name_crc
    - ?: [52 69 63 6B 52 6F 6C 6C 5F 44 2D 6F 48 67 35 53 4A 59 52 48 41 30 2E 6D 6B 76]: nombre del fichero, RickRoll_D-oHg5SJYRHA0.mk

 jint16_t magic;
        jint16_t nodetype;      / == JFFS2_NODETYPE_DIRENT /
        jint32_t totlen;
        jint32_t hdr_crc;
        jint32_t pino;
        jint32_t version;
        jint32_t ino; / == zero for unlink /
        jint32_t mctime;
        uint8_t nsize;
        uint8_t type;
        uint8_t unused[2];
        jint32_t node_crc;
        jint32_t name_crc;

  - Ejemplo 2

19 85 E0 01 00 00 00 2C 52 4F 46 4C 00 00 00 01 00 00 00 01 00 00 00 03 5C 1B 5F 34 0C 08 00 00 2F 73 65 63 72 65 74 2F 66 6C 61 67

    - 2 [19 85]: magic numbers del nodo, valor fijo
    - 2 [E0 01]: tipo dirent, valor fijo
    - 4 [00 00 00 2C]: tamanyo del inodo, 66 bytes
    - 4 [52 4F 46 4C]: header CRC checksum del nodo
    - 4 [00 00 00 01]: pino, 1
    - 4 [00 00 00 01]: version, 1
    - 4 [00 00 00 03]: ino (=0 para desenlazar), 3
    - 4 [5C 1B 5F 34]: mctime
    - 1 [0C]: nsize
    - 1 [08]: type
    - 1 [00 00]: unused[2]
    - 4 [?? ?? ?? ??]: node_crc
    - 4 [?? ?? ?? ??]: name_crc
    - ?: [2F 73 65 63 72 65 74 2F 66 6C 61 67]: nombre del fichero, /secret/fla

  - Solucion

19 85 E0 02 00 00 00 44 72 6F 6C 66 00 00 00 03 00 00 00 01 00 00 81 A4 00 00 00 00 00 00 06 68 5C 1B 5F 34 5C 1B 5F 34 5C 1B 5F 34 00 00 00 00 00 00 00 4B 00 00 06 68 06 00 00 00 68 6F 6E 64 61 63 72 63 3C 33 32 36 75 36 8E CF 4D CC 4D 8C 2F CF 48 2C 89 CF 48 2C 28 48 CD 4B 89 2F C9 8F CF AD 8C CF C8 CF 4B 49 8C 77 0E D2 75 E6 32 1E 55 37 1A 2E A3 E9 60 34 7F 8C 96 07 A3 E5 24 91 F5 02 40 00 21 D7 1F 00 01 06 00 0E 9A 54 10

struct jffs2_raw_inode
{
    19 85 jint16_t magic;      /* A constant magic number. */
    E0 02 jint16_t nodetype;   /* == JFFS_NODETYPE_INODE */
    00 00 00 44 jint32_t totlen;     /* Total length of this node (inc data, etc.) */
    72 6F 6C 66 jint32_t hdr_crc;    /* Crc checksum */
    00 00 00 03 jint32_t ino;        /* Inode number.  */
    00 00 00 01 jint32_t version;    /* Version number.  */
    00 00 81 A4 jmode_t mode;        /* The file's type or mode.  */
    00 00 jint16_t uid;        /* The file's owner.  */
    00 00 jint16_t gid;        /* The file's group.  */
    00 00 06 68 jint32_t isize;      /* Total resultant size of this inode (used for truncations)  */
    5C 1B 5F 34 jint32_t atime;      /* Last access time.  */
    5C 1B 5F 34 jint32_t mtime;      /* Last modification time.  */
    5C 1B 5F 34 jint32_t ctime;      /* Change time.  */
    00 00 00 00 jint32_t offset;     /* Where to begin to write.  */
    00 00 00 4B jint32_t csize;      /* (Compressed) data size */
    00 00 06 68 jint32_t dsize;      /* Size of the node's data. (after decompression) */
    06 uint8_t compr;       /* Compression algorithm used */ #define JFFS2_COMPR_ZLIB	0x06
    00 uint8_t usercompr;   /* Compression algorithm requested by the user */
    00 00 jint16_t flags;      /* See JFFS2_INO_FLAG_* */
    68 6F 6E 64 jint32_t data_crc;   /* CRC for the (compressed) data.  */
    61 63 72 63 jint32_t node_crc;   /* CRC for the raw inode (excluding data)  */
    uint8_t data[0];

- JFFS2_NODETYPE_INODE (ino) (valor: 0xE002)
  - Se define como "Inode data"
  - Soporta enlaces duros para evitar duplicidad de datos
  - Parametros que contiene:
    - Metadatos
      - ID de usuario
      - ID de grupo
      - Permisos de usa
      - Y mas datos...
    - Tamanyo actual del inode
    - Mas datos opcionales
  - Parametros que NO contiene:
    - ID del inode padre
    - Nombre del fichero al que esta asociado
  - Si no esta asociado a un file descriptor o dirent se borrara
  - Sus datos se comprimen con los algoritmos: zero, one o zlib
- JFFS2_NODETYPE_CLEANMARKER
  - Marcador que identifica un bloque que ha sido borrado y es apto
    para volver a almacenar nuevos datos.
  - Se escribe al inicio de un bloque

inode != dirent

El proceso de montaje del filesystem se divide en 4 pasos:
- Escaneo para obtener informacion de todos los nodos
- Crear mapas de datos y calcular el "nlink" de cada inode
- Borrar los inodes con nlink == 0
- Borrar la informacion temporal cacheada

----------
Existe CRC para varios tipos de nodos:
- todos los headers de los nodos
- en contenido de los nodos
- en el data de los nodos
- en el campo de nombre de los nodos
