#### RETO

![SPIF! SPIF! SPIF!](reto.png)

#### OBTENCIÓN DE INFORMACIÓN

Ejecuto binwalk para ver si contiene algún archivo:

`~$ binwalk -v SPIFFED`

OUTPUT:

![BINWALK OUTPUT](binwalk-output.png)

Consulto **man arj** para averiguar cómo se descomprime un archivo **arj** y ejecuto el comando:

`~$ arj e SPIFFED`

La consola devuelve un aviso de que **ARJ** no ha encontrado ningúna archivo que se llame `SPIFFED.arj`, por lo que renombro el archivo a `SPIFFED.arj` y vuelvo a ejecutar el comando.

Ahora obtengo el error `BAD HEADER`. Con una rápida búsqueda sobre cuáles son los números mágicos de ARJ encuentro que son `60 EA` ([lista file signatures](https://www.garykessler.net/library/file_sigs.html)).

Compruebo qué bytes tiene como cabecera el archivo SPIFFED con:

`~$ hexdump -C SPIFFED | head`

Y, efectivamente, sus dos primeros bytes son, **erróneamente**, `60 FA`.

Corrijo la cabecera con el **hex editor** okteta.

Vuelvo a intentar extraer los archivos con `arj` y me extrae cuatro archivos de texto:

```
file_0.txt
file_1.txt
file_2.txt
file_3.txt
```

Analizando cada archivo de txt, no encuentro ninguna pista sobre lo que podrían ser. Vuelvo a leer el enunciado del reto, que dice **"Mr. Zeus messup up with my images and gave me this. Plez help me... :(" y el nombre del reto "SPIF! SPIF! SPIF!"**.

Pienso que **SPIF** podría ser una pista y busco información en internet. Al final, acabo encontrando [esta](http://fhtr.org/multires/spif/spif.html) página, la cual explica que SPIF es una formato de imagen.

Tiene sentido. Sigo buscando en internet y me encuentro con [este](https://github.com/MrZeusGaming/SPIF-image-encryption) repositorio de github. El nombre del usuario es **MrZeusGaming**... todo parece indicar que voy por buen camino.

Clono el repositorio y leyendo las instrucciones del archivo `README.md`, sé que la instrucción para desencriptar una imagen en formato SPIF es:

`~$ python3.5 SPIF.py decrypt file_0.txt DETF.py`

Y obtengo una imagen de extensión `.png`.

Procedo a desencriptar todos los archivos de texto y encuentro la flag en la imagen que estaba encriptada en **file_2.txt**.

