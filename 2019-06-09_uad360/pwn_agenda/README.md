# Agenda (1 solve)

    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      No PIE
    Libc:     libc-2.27.so


## Introducción
Debido a que hemos sido el único equipo en resolver este desafío, me siento motivado a
crear este writeup con el objetivo de que sirva como una introducción a _heap exploitation_ y
de saciar la curiosidad de aquellos que lo intentaron y que siguen dándose cabezazos en la
pared intentando comprender que está fallando o que no están viendo.

Además, para aquellos que todavía no se sientan cómodos con el _heap_ y con el algoritmo malloc,
he dejado unos pocos links al final de este write-up que os servirán como introducción.

Espero que este documento os sea de gran ayuda.

## Reconocimiento
Como en cualquier otro desafío de comenzamos recolectando tanta información como
sea posible de nuestro _target_, en nuestro caso el binario.

Antes de nada, nos fijamos en que el programa consta de 4 funciones básicas:

1. Crear evento
2. Ver evento
3. Editar evento
4. Eliminar evento

Cuando llamemos a la función `create_event()` el programa realizará lo siguiente:

1. Comprobar que se han creado 0xd eventos o menos
1. Pedir la longitud del evento al usuario
1. Llamar a malloc con este valor
1. Almacenar la dirección del `chunk` recién creado y su longitud en un array en el `.bss`
1. Ejecutar un `read(0, chunk, length)` que almacena en el `chunk` un _string_ (El evento)

Por otro lado, si ejecutamos la función `show_event()` ocurrirá lo siguiente:

1. Pedir al usuario el ID del evento
1. Ejecutar `write(1, chunk, length)` pasando como 3er parametro la longitud con la que se realizó la petición a `malloc()` (almacenada en el array del `.bss`)

En el caso de que llamemos a la función `remove_event()` ocurrirá lo siguiente:

1. Pedir al usuario el ID del evento
1. Ejecutar `free(chunk)`
1. En el array almacenado en el `.bss` sobreescribirá el puntero del evento con NULL

Y por último, si llamamos a la función `edit_event()` oduccrirá lo siguiente:

1. Pedir al usuario el ID del evento
1. Comprobar que el puntero del evento no es NULL (es decir, que no está `free()`'d)
1. Recuperar la longitud del evento del array en `.bss`
1. Ejecutar `read(0, chunk, length)`

## Filtración
Sinceramente, no tenía ni idea de como llamar a este apartado. Pero para que os hagáis una idea,
simplemente consiste en conseguir filtrar una dirección de memoria que nos sirva para calcular
la dirección base de `libc` y así poder saltarnos el ASLR.

Para ello nos aprovecharemos del bug que hay en `show_event()`.

Como ya hemos visto en el apartado de **Reconocimiento** `show_event()`, a la hora de imprimir en pantalla
el contenido de nuestro evento, sencillamente coge la longitud con la que fue hecho el `malloc()` y felizmente
ejecuta `write(1, chunk, length)`. El error aquí es bastante claro, a la hora de imprimir lo que sea en pantalla,
especialmente información que ha sido introducida por un usuario, el programa debe añadir un `null-byte` al final
del _string_ y asegurarse de solo imprimir hasta este `null-byte`. En nuestro caso, no solo no se añade un `null-byte`,
sino que ni siquiera se preocupa de imprimir tan solo el _input_ del usuario y por lo tanto un _leak_ puede ocurrir.

Lo que debemos preguntarnos ahora es: ¿Cómo podemos colocar información sensible del proceso en un evento que nos sirva para _bypassear_ a ASLR?

La respuesta resulta ser bastánte sencilla, aunque requiere de ciertos conocimientos del algoritmo malloc:

Cuando un `chunk` se libera (`free()`) se introduce en lo que llamamos un `linked-list` (a veces `double linked-list`) de manera que utiliza sus primeros 0x8 bytes,
y en ocasiones sus primeros 0x10 para _señalar_ al siguiente (y en algunos casos al anterior) chunk liberado.

Esto quiere decir que cuando liberemos un chunk, en el caso de que ya haya otro liberado, el algoritmo malloc posicionará un puntero
en los primeros 0x8 (y a veces también en 0x10) bytes.

El tipo de puntero que posicione en el evento liberado, depende del tamaño del chunk, algo que por suerte tenemos bajo control. Y también
sabemos que en el caso de que este chunk sea bastante grande (unos 2048 bytes) y el chunk justo después de este no sea ni el `top` chunk ni otro
chunk igual o más grande libre, malloc almacenará en sus primeros 8 y 0x10 bytes un puntero a `main_arena`, un estructura que se
encuentra mapeada dentro de `libc.so` y que por lo tanto nos sirve para _bypassear_ ASLR.

Dicho todo esto, para conseguir _bypassear_ a ASLR debemos:

1. Crear un evento con una longitud de 2048 bytes o superior.
1. Crear otro evento para que cuando liberemos el anterior no se fusione con el `top` chunk
1. Liberar el primer evento
1. Crear otro evento para que se posicione en el espacio de memoria donde estaba previamente el evento grande (no es necesario que el evento nuevo sea igual de grande que el viejo)
1. Almacenar 8 bytes o menos en este evento para poder leer posteriormente el puntero que nos interesa
1. Mostrar este último evento para conseguir un `leak` de libc.
1. Calcular el offset desde este puntero al inicio de `libc.so` y restárselo. (el offset será el mismo siempre y cuando se utilice la misma versión de libc)


## Explotación
Para conseguir controlar el flujo de ejecución del programa, debíamos fijarnos en lo siguiente:

Como he dicho previamente, los punteros de los eventos y sus respectivas longitudes se almacenan en el segmento `.bss` de la memoria y además, están separados
por 0x50 bytes. Aquí tenéis un ejemplo de 2 eventos creados:

    0x6020a0 <my_event_list>:	0x0000000002053270	0x0000000002053290   <--- Eventos 1 y 2
    0x6020b0 <my_event_list+16>:	0x0000000000000000	0x0000000000000000
    0x6020c0 <my_event_list+32>:	0x0000000000000000	0x0000000000000000
    0x6020d0 <my_event_list+48>:	0x0000000000000000	0x0000000000000000
    0x6020e0 <my_event_list+64>:	0x0000000000000000	0x0000000000000000
    0x6020f0 <my_event_list+80>:	0x0000001800000018	0x0000000000000000   <--- Longitudes 1 y 2 (32 bits)
    0x602100 <my_event_list+96>:	0x0000000000000000	0x0000000000000000
    0x602110 <my_event_list+112>:	0x0000000000000000	0x0000000000000000

Si ahora tenemos en cuenta cuantos eventos podemos crear (0xd) nos damos cuenta de que podemos sobreescribir las primeras 6 longitudes (32-bit) con los últimos 3 punteros
de los eventos!!

Ejemplo:

    0x6020a0 <my_event_list>:	0x0000000002053270	0x0000000002053290   <--- Eventos 1 y 2
    0x6020b0 <my_event_list+16>:	0x0000000000000000	0x0000000000000000
    0x6020c0 <my_event_list+32>:	0x00000000020532d0	0x00000000020532b0
    0x6020d0 <my_event_list+48>:	0x0000000000000000	0x0000000002054300
    0x6020e0 <my_event_list+64>:	0x00000000020532f0	0x0000000002053310
    0x6020f0 <my_event_list+80>:	0x0000000002053330	0x0000001800000018   <--- Longitudes 1 y 2 sobreescritas con un puntero
    0x602100 <my_event_list+96>:	0x0000001800000018	0x0000001800001000
    0x602110 <my_event_list+112>:	0x0000001800000018	0x0000000000000018

Esto significa que ahora, en el caso de que intentemos editar el primer evento, podremos escribir muchos más bytes de los que deberíamos, causando así
un `Heap Buffer Overflow`.

Lo siguiente que nos debemos preguntar es: ¿Que puedo sobreescribir en el heap?

La respuesta a esta pregunta, una vez más, es relativamente sencilla si conocemos el algoritmo malloc.

Como ya vimos en el apartado de **Filtración**, los chunks libres están ordenados en un `linked-list`. En el caso de que pudiéramos modificar un nodo
dentro de este `linked-list` (un chunk libre), podríamos insertar otro chunk o dirección de memoria en el `linked-list`, y por lo tanto, cuando malloc recibiera
una petición, podría acabar devolviendo una dirección de memoria arbitrária, lo que causaría un preciado `read-write primitive` (TL;DR Podemos escribir a la región de memoria que queramos)

Y bueno, tenemos la suerte de que el binario es `Partial RELRO` lo que significa que podemos modificar entradas GOT, así que es hora de elegir nuestra función víctima.

Llegados a este punto, muchos estaréis tentados a sobreescribir `exit@got` debido a que podemos llamar a la función de manera muy controlada, pero no es ni de
lejos la mejor opción. Otra opción mucho mejor es la de sobreecribir `free@got` con `__libc_system` debido a que en este caso ambas funciones reciben como primer y único parámentro un string.

Así que ahora tan solo tenemos que crear un evento que contenta el string `/bin/sh\0` y una vez `free@got` haya sido sobreescrito por `__libc_system` borrar el evento y
disfrutar de nuestra _shell_.

## Conclusión
Una vez más, espero que este write-up os haya sido de ayuda. Para su mejor comprensión, os recomiendo que os lo volvais a leer, esta vez
con el exploit al lado ejecutando y analizando el proceso con `gdb`.

Además os dejo algunos links que me sirvieron a mi y que espero que os sirvan a vosotros para introduciros en el mundo de `Heap Exploitation`:

1. https://sploitfun.wordpress.com/2015/02/10/understanding-glibc-malloc/
1. http://www.phrack.org/issues/66/10.html
1. https://gbmaster.wordpress.com/2014/08/24/x86-exploitation-101-this-is-the-first-witchy-house/
1. https://github.com/shellphish/how2heap

_Sí, los links están en orden por algo ;)_


Suerte!!


