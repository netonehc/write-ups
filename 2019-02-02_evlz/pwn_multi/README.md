# Multi (500p)

En este desafío deberemos avanzar por las distintas fases que tiene (1-4) hasta conseguir una shell.

Cada una de las fases tiene una vulnerabilidad que deberemos explotar.

# Stage 1

Esta es probablemente la fase más sencilla de todas, el programa nos pide dos numeros y comprueba que no sean mayores que 1336, después resta el
segundo menos el primero y en el caso de que el resultado sea 1337 te dirige al siguiente _stage_.

Por lo tanto el input sería: `-1 1336`

# Stage 2

Esta fase requiere tener una visión muy clara de como se está ejecutando el programa. 

    0x00000000004014d2 <+0>:	push   rbp
    0x00000000004014d3 <+1>:	mov    rbp,rsp
    0x00000000004014d6 <+4>:	sub    rsp,0x10
    0x00000000004014da <+8>:	lea    rax,[rbp-0x8]
    0x00000000004014de <+12>:	mov    ecx,0x0
    0x00000000004014e3 <+17>:	lea    rdx,[rip+0xffffffffffffff19]        # 0x401403 <operations>
    0x00000000004014ea <+24>:	mov    esi,0x0
    0x00000000004014ef <+29>:	mov    rdi,rax
    0x00000000004014f2 <+32>:	call   0x401040 <pthread_create@plt>
    0x00000000004014f7 <+37>:	lea    rax,[rbp-0x10]
    0x00000000004014fb <+41>:	mov    ecx,0x0
    0x0000000000401500 <+46>:	lea    rdx,[rip+0xfffffffffffffebe]        # 0x4013c5 <check>
    0x0000000000401507 <+53>:	mov    esi,0x0
    0x000000000040150c <+58>:	mov    rdi,rax
    0x000000000040150f <+61>:	call   0x401040 <pthread_create@plt>
    0x0000000000401514 <+66>:	mov    rax,QWORD PTR [rbp-0x8]
    0x0000000000401518 <+70>:	mov    esi,0x0
    0x000000000040151d <+75>:	mov    rdi,rax
    0x0000000000401520 <+78>:	call   0x4010c0 <pthread_join@plt>
    0x0000000000401525 <+83>:	mov    eax,DWORD PTR [rip+0x2b65]        # 0x404090 <maxval>
    0x000000000040152b <+89>:	test   eax,eax
    0x000000000040152d <+91>:	jns    0x401540 <stage2+110>
    0x000000000040152f <+93>:	mov    DWORD PTR [rip+0x2b97],0x1        # 0x4040d0 <stage2status>
    0x0000000000401539 <+103>:	mov    eax,0x1
    0x000000000040153e <+108>:	jmp    0x401545 <stage2+115>
    0x0000000000401540 <+110>:	mov    eax,0x0
    0x0000000000401545 <+115>:	leave  
    0x0000000000401546 <+116>:	ret 

Aquí podemos ver que se crean 2 threads, 1 que ejecuta `operations()` y otro `check()`. 

`operations()` será el responsable de
mostrar en pantalla el menú, y nos permitirá realizar una serie de operaciones sobre una variable compartida entre los threads llamada `maxval`.

El thread de `operations()` nos dará la opción de sumar o restarle valores a esta variable global (entre 0 y 250). Esta misma función establece
un valor máximo que puede contener esta variable (10000), pero no un valor mínimo. De esto último se encarga el thread de `check()`, que
cada vez que detecte que el valor de la variable está por debajo de 5000, le sumará 4900.

Debemos tener en cuenta que tan solo pasaremos a la siguiente fase en el caso de que el valor de `maxval` sea menor que 0.

Para ello optaremos por conseguir hacer salir al thread the `check()` para así poder restar todas las veces que queramos. Pero, ¿cómo conseguimos esto?

La clave era fijarse en que ambos threads, al final de su ciclo hacen una llamada a `sleep()`, con la única diferencia en que `operations()` se demora
1 segundo, y `check()` se demora 4. Por lo tanto, podríamos restar hasta llegar a un valor algo más pequeño que 5000, y rápidamente sumarle lo suficiente
para que `maxval + 4900 > 10000` y que por lo tanto, el thread de `check()` muera. Este tipo de vulnerabilidad se conoce como _Race Condition_ o Condición de Carrera.

Despues de esto podemos tranquilamente ir restando hasta conseguir que `maxval` sea menor que 0, y por lo tanto, pasar a la siguiente fase.


# Stage 3

En esta fase hay dos inputs, 1 que te pide tu nombre, y otro que te pregunta por la dirección de `_IO_puts`.

Por suerte, el primer input es vulnerable debido a que se le están pasando los argumentos a printf de manera errónea

    0x00000000004015b0 <+105>:	lea    rax,[rbp-0xe0]     # Nuestro buffer en el stack.
    0x00000000004015b7 <+112>:	mov    rdi,rax            # Se pasa como primer argumento
    0x00000000004015ba <+115>:	mov    eax,0x0
    0x00000000004015bf <+120>:	call   0x401070 <printf@plt>

La traducción a C sería:

    printf(buffer);

Cuando lo correcto sería:

    printf("%s\n", buffer);

Esto quiere decir que tenemos un FSE en el stack. Por lo tanto, la forma de conseguir la dirección de `_IO_puts` será añadir
la dirección de `puts@got` al final de nuestro payload, y acceder a los contenidos de esa dirección con _format strings_.

La estructura del payload es la siguiente:

    payload = "AAAA"    # Padding
    payload += "%9$s"   # Accede al contenido de la dirección de memoria y lo trata como (char *)
    payload += p64(elf.got["puts"])     # La dirección de puts@got

Despues tan solo hace falta _parsear_ el output para poder calcular la dirección de puts.

Con `pwntools` es relativamente sencillo:

    leak = u64(p.recvuntil("\xe8\x3f\x40", drop=True).ljust(8, "\x00"))

Llegados a este punto tan solo hace falta enviar al programa el valor en decimal de `_IO_puts`.


# Stage 4

En esta última fase nos presentan un menú con 4 opciones:

    1. Create a memory region
    2. Delete the memory region
    3. Do some captcha
    4. Jump

Empecemos analizando estas funciones de abajo a arriba.

- `Jump` hará un _call_ a la dirección que contiene OTRA dirección que contiene una variable que llamaremos `ptr`.

- `Do some captcha` hará un _call_ a `malloc()` y guardará EN la dirección de memoria devuelta por `malloc` un
número que dará como input el usuario.

- `Delete memory region` hará un `free()` a la dirección de memoria que contenga `ptr`.

- `Create a memory region` hará un _call_ a `malloc()`, guardará la dirección de memoria de `exit` en los primeros 8 bytes del
contenido de la dirección devuelta por `malloc` y escribirá la dirección devuelta por `malloc` en la variable `ptr`.


`Do some captcha` y `Create a memory region` realizan una petición a `malloc()` del mismo tamaño, lo que significa, que
si un _chunk_ es liberado y se almacena otro, este último será almacenado en la misma dirección de memoria que el anterior.
Y como `Delete memory region` no limpia la variable `ptr` podemos:

- `Create a memory region` para guardar la dirección de memoria en `ptr`.
- `Delete a memory region` para liberar esa dirección de memoria.
- `Do some captcha` para ocupar otra vez esa dirección de memoria, pero esta vez podremos escribir el valor que nosotros queramos. En nuestro caso la dirección de memoria de `finish`
- `Jump` para saltar a la dirección de memoria que hemos almacenado.

Una vez hayamos saltado a `finish` el programa ejecutará un `system()` y tendremos una shell.




