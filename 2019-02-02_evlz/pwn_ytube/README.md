# Ytube (1000p)
Este desafío es algo diferente a lo que estamos acostumbrados en la mayoría de CTFs.

En este caso se nos proporcionaba un binario, y debíamos demostrar que habíamos logrado RCE
abriendo una calculadora en _streaming_.

# Reconnaissance

    0x080491fb <+50>:	call   0x8049050 <fgets@plt>
    0x08049200 <+55>:	add    esp,0x10
    0x08049203 <+58>:	sub    esp,0xc
    0x08049206 <+61>:	mov    eax,0x804c060    # Nuestro buffer
    0x0804920c <+67>:	push   eax
    0x0804920d <+68>:	call   0x8049040 <printf@plt>

Si nos fijamos en esta pieza del código, rápidamente podemos observar que se le están pasando
los argumentos a `printf` de manera errónea.

La traducción a C sería:

    fgets(buffer, size, stdin);
    printf(buffer);

Cuando lo correcto sería:

    fgets(buffer, size, stdin);
    printf("%s\n", buffer);

Esto quiere decir que tenemos un FSE que nos permite escribir a cualquier dirección de memoria... o no...

# Debugging

Todo parecía fácil y sencillo hasta que nos encontramos con el verdadero problema del desafío.

Lo que se suele hacer cuando tienes un `format string vulnerability` es sobreescribir una entrada GOT
con un `gadget`, o en nuestro caso, con la función `win` que nos facilitaban, a la
que podíamos llamar para poder ejecutar un comando, en nuestro caso: `/snap/bin/gnome-calculator` para
_spawnear_ la calculadora.

De hecho, sobreescribir `printf@got` nos vendría genial, ya que como vemos en la pieza de asm que he
puesto antes, antes del printf se hace un `push` con la dirección que contiene nuestro input, y por
lo tanto, tras haber sobreescrito `printf@got` podemos simplemente enviar el comando que queremos
ejecutar.

El problema, es que para hacer esto necesitamos tener nuestro _buffer_ en el stack, sin embargo, en nuestro
caso está en el _bss_ area.

La solución a este problema era escribir la dirección de la entrada GOT que queríamos sobreescribir al stack,
y posteriormente sobreescribir los contenidos de la dirección de memoria que hemos escrito al stack.

# Exploiting

Para realizar lo anterior, necesitamos encontrar en el stack un puntero a otra dirección del stack.

![](https://gitlab.com/netonehc/write-ups/raw/master/2019-02-02_evlz/pwn_ytube/screenshots/stack_addr.png)

La dirección marcada parece una buena opción.

Dado que `printf@got` se encuentra en `0x804c00c (134529036 en decimal)` y la dirección que queremos sobreescribir
está en el _offset_ 13, para sobreescribir la dirección del stack con ese valor deberemos enviar como
input: `%13$134529036x%13$n`.

Bien, ahora ya tenemos la dirección de `printf@got` en el stack, ahora solo falta sobreescribir los contenidos de 
esa dirección con la dirección de `win` que se encuentra en `0x80491a2 (134517154 en decimal)`. La dirección a la
cual escribimos antes resulta estar en el _offset_ 49 en el stack, por lo tanto el _payload_ se queda: `%49$134517154%49$n`.

Ahora tan solo queda enviar el comando que queremos ejecutar y listo !!

